#!/bin/bash
#This script downloads the latest keystore 
#first rename existing keystore
mv nubot_keystore.jks nubot_keystore_old.jks
#then download from repository (branch develop)
curl -L -O https://bitbucket.org/JordanLeePeershares/nubottrading/raw/develop/res/ssl/nubot_keystore.jks