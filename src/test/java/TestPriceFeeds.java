/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.pricefeeds.PriceFeedManager;
import com.nubits.nubot.utils.InitTests;
import com.nubits.nubot.utils.Utils;
import junit.framework.TestCase;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


public class TestPriceFeeds extends TestCase {
    private static final Logger LOG = LoggerFactory.getLogger(TestPriceFeeds.class.getName());

    @Test
    public void testTrackBTC() {
        init();
        String mainFeed = CurrencyList.BTC.getDefaultMainFeedName();
        ArrayList<String> backupFeedList = new ArrayList<>();
        backupFeedList = CurrencyList.BTC.getDefaultbackupFeedNames();
        execute(mainFeed, backupFeedList, CurrencyList.BTC_USD);

    }

    @Test
    public void testTrackPPC() {
        init();
        String mainFeed = CurrencyList.PPC.getDefaultMainFeedName();
        ArrayList<String> backupFeedList = new ArrayList<>();
        backupFeedList = CurrencyList.PPC.getDefaultbackupFeedNames();

        execute(mainFeed, backupFeedList, CurrencyList.PPC_USD);
    }

    @Test
    public void testTrackLTC() {
        init();
        String mainFeed = CurrencyList.LTC.getDefaultMainFeedName();
        ArrayList<String> backupFeedList = new ArrayList<>();
        backupFeedList = CurrencyList.LTC.getDefaultbackupFeedNames();

        execute(mainFeed, backupFeedList, CurrencyList.LTC_USD);
    }

    @Test
    public void testTrackETH() {
        init();
        String mainFeed = CurrencyList.ETH.getDefaultMainFeedName();
        ArrayList<String> backupFeedList = new ArrayList<>();
        backupFeedList = CurrencyList.ETH.getDefaultbackupFeedNames();

        execute(mainFeed, backupFeedList, CurrencyList.ETH_USD);
    }

    @Test
    public void testTrackXRP() {
        init();
        String mainFeed = CurrencyList.XRP.getDefaultMainFeedName();
        ArrayList<String> backupFeedList = new ArrayList<>();
        backupFeedList = CurrencyList.XRP.getDefaultbackupFeedNames();

        execute(mainFeed, backupFeedList, CurrencyList.XRP_USD);
    }

    @Test
    public void testTrackEUR() {
        init();
        String mainFeed = CurrencyList.EUR.getDefaultMainFeedName();
        ArrayList<String> backupFeedList = new ArrayList<>();
        backupFeedList = CurrencyList.EUR.getDefaultbackupFeedNames();

        execute(mainFeed, backupFeedList, CurrencyList.EUR_USD);
    }

    @Test
    public void testTrackJPY() {
        init();
        String mainFeed = CurrencyList.JPY.getDefaultMainFeedName();
        ArrayList<String> backupFeedList = new ArrayList<>();
        backupFeedList = CurrencyList.JPY.getDefaultbackupFeedNames();

        execute(mainFeed, backupFeedList, CurrencyList.JPY_USD);
    }

    @Test
    public void testTrackHKD() {
        init();
        String mainFeed = CurrencyList.HKD.getDefaultMainFeedName();
        ArrayList<String> backupFeedList = new ArrayList<>();
        backupFeedList = CurrencyList.HKD.getDefaultbackupFeedNames();

        execute(mainFeed, backupFeedList, CurrencyList.HKD_USD);
    }

    @Test
    public void testTrackPHP() {
        init();
        String mainFeed = CurrencyList.PHP.getDefaultMainFeedName();
        ArrayList<String> backupFeedList = new ArrayList<>();
        backupFeedList = CurrencyList.PHP.getDefaultbackupFeedNames();

        execute(mainFeed, backupFeedList, CurrencyList.PHP_USD);
    }

    @Test
    public void testTrackCNY() {
        init();
        String mainFeed = CurrencyList.CNY.getDefaultMainFeedName();
        ArrayList<String> backupFeedList = new ArrayList<>();
        backupFeedList = CurrencyList.CNY.getDefaultbackupFeedNames();

        execute(mainFeed, backupFeedList, CurrencyList.CNY_USD);
    }

    private void init() {
        InitTests.loadCredentialManager(false);
        LOG.info("Set up SSL certificates");
        Utils.installKeystore(false);
    }

    private void execute(String mainFeed, ArrayList<String> backupFeedList, CurrencyPair pair) {

        PriceFeedManager pfm = null;
        try {
            pfm = new PriceFeedManager(mainFeed, backupFeedList, pair);
        } catch (NuBotConfigException e) {
            LOG.error(e.toString());
            fail(e.toString());
        }

        pfm.fetchLastPrices();
        ArrayList<LastPrice> priceList = pfm.getPriceBatch().priceList;

        LOG.info("\n\n\n ---------------------- Testing results for: " + pair.toStringSepSpecial("/"));
        LOG.info("response from " + priceList.size() + "/" + pfm.getFeedList().size() + " feeds\n");

        for (int i = 0; i < priceList.size(); i++) {
            LastPrice tempPrice = priceList.get(i);
            if (tempPrice.isError()) {
                fail(tempPrice.toString());
            }
            LOG.info(tempPrice.getSource() + ":1 " + tempPrice.getCurrencyMeasured().getCode() + " = "
                    + tempPrice.getPrice().getQuantity() + " " + tempPrice.getPrice().getCurrency().getCode());
        }
    }
}