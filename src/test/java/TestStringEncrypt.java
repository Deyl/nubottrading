/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import com.nubits.nustringencrypt.StringEncrypt;
import junit.framework.TestCase;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * author mj2p
 * https://bitbucket.org/mj2p/nustringencrypt
 */
public class TestStringEncrypt extends TestCase {

    private static final Logger LOG = LoggerFactory.getLogger(TestStringEncrypt.class.getName());

    StringEncrypt stringEncrypt = new StringEncrypt();

    @Test
    public void testEncrypt() throws Exception {

        //build a test array of strings typical to NuBot options
        String[] testStrings = new String[3];

        testStrings[0] = "barry@example.com";
        testStrings[1] = "BMJ2PnJqZjpDvttJMZTEBiCZzsGL9AL5fE";
        testStrings[2] = "SMJ2PX3KoPbxPqe4o873yCLQQNCjeiiJi9";

        //encrypt them
        String[] outputs = stringEncrypt.encrypt(testStrings, "passPhrase");

        for (String output : outputs) {
            System.out.println(output);
        }
        //make sure the appear correctly
        assertEquals(outputs[0], "e995IIqiQiI=:jQaZHtLUNrpzBN48bgVXrf3mHzSYEOV7hHb+ndfr7FA=");
        assertEquals(outputs[1], "jrBN6NDy7oA=:ZhLm5U6SuCcoAKNzR7/5p/XT9zEFRTyrYLX0ooP91XeCRTG5BGifjQusJ+spdmh9");
        assertEquals(outputs[2], "sDa4G1G3mdk=:IkpMi+v+snGUGlbLFbyul4ujY/iYOA7UOklYbO5WT7NA6duCJneCZYD+1a8mcC9u");

    }

    @Test
    public void testDecrypt() throws Exception {

        String[] testStrings = new String[3];


        testStrings[0] = "e995IIqiQiI=:jQaZHtLUNrpzBN48bgVXrf3mHzSYEOV7hHb+ndfr7FA=";
        testStrings[1] = "jrBN6NDy7oA=:ZhLm5U6SuCcoAKNzR7/5p/XT9zEFRTyrYLX0ooP91XeCRTG5BGifjQusJ+spdmh9";
        testStrings[2] = "sDa4G1G3mdk=:IkpMi+v+snGUGlbLFbyul4ujY/iYOA7UOklYbO5WT7NA6duCJneCZYD+1a8mcC9u";

        String[] outputs = stringEncrypt.decrypt(testStrings, "passPhrase");
        /*for (String output : outputs) {
            System.out.println(output);
        }*/

        assertEquals(outputs[0], "barry@example.com");
        assertEquals(outputs[1], "BMJ2PnJqZjpDvttJMZTEBiCZzsGL9AL5fE");
        assertEquals(outputs[2], "SMJ2PX3KoPbxPqe4o873yCLQQNCjeiiJi9");

    }

    @Test
    public void testBadDecryption() throws Exception {
        String testString = "BMJ2PnJqZjpDvttJMZTEBiCZzsGL9AL5fE";
        String encryptedString = stringEncrypt.encrypt(testString, "PassPhrase");
        assertEquals(encryptedString, "jrBN6NDy7oA=:mYVsraaX1pOs36laUFbUZJxsyMCTmsgI6cgL2TYfJJZdnU10EulZ4cfd+fCzsrfX");
        String decryptedString = stringEncrypt.decrypt(encryptedString, "NotThePassPhrase");
        assertEquals(decryptedString, "The decryption failed. Please check your passphrase");
    }
}
