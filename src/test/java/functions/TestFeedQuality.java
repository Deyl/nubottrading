/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package functions;

import com.nubits.nubot.models.Amount;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.pricefeeds.FeedQuality;
import com.nubits.nubot.pricefeeds.PriceBatch;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;

public class TestFeedQuality extends TestCase {

    @Test
    public void testok() {

        ArrayList<LastPrice> currentPriceList = new ArrayList<>();
        LastPrice lp = new LastPrice(false, "A", CurrencyList.NBT, new Amount(100, CurrencyList.NBT));
        LastPrice lp2 = new LastPrice(false, "B", CurrencyList.NBT, new Amount(100, CurrencyList.NBT));
        currentPriceList.add(lp);
        currentPriceList.add(lp2);
        boolean ok = FeedQuality.sanityCheck(0.01, new PriceBatch(currentPriceList), 0);
        assertTrue(ok);

    }

    @Test
    public void testFail() {
        ArrayList<LastPrice> currentPriceList = new ArrayList<>();
        LastPrice lp = new LastPrice(false, "A", CurrencyList.NBT, new Amount(100, CurrencyList.NBT));
        LastPrice lp2 = new LastPrice(false, "B", CurrencyList.NBT, new Amount(200, CurrencyList.NBT));
        currentPriceList.add(lp);
        currentPriceList.add(lp2);
        boolean ok = FeedQuality.sanityCheck(0.01, new PriceBatch(currentPriceList), 0);
        assertTrue(!ok);

    }

    @Test
    public void testOkNormal() {
        ArrayList<LastPrice> currentPriceList = new ArrayList<>();
        LastPrice lp = new LastPrice(false, "A", CurrencyList.NBT, new Amount(100, CurrencyList.NBT));
        LastPrice lp2 = new LastPrice(false, "B", CurrencyList.NBT, new Amount(200, CurrencyList.NBT));
        LastPrice lp3 = new LastPrice(false, "B", CurrencyList.NBT, new Amount(100, CurrencyList.NBT));
        currentPriceList.add(lp);
        currentPriceList.add(lp2);
        currentPriceList.add(lp3);

        LastPrice good = FeedQuality.handleNormal(0.01, new PriceBatch(currentPriceList));
        double q = good.getPrice().getQuantity();
        assertTrue(good.getPrice().getQuantity() == 100.0);
        //assertTrue(!ok);

    }

    @Test
    public void testFailMain() {
        //mainfeed = 0.5 ,   backup 1 = 100, backup 2=101

        ArrayList<LastPrice> currentPriceList = new ArrayList<>();
        LastPrice lp = new LastPrice(false, "A", CurrencyList.NBT, new Amount(0.5, CurrencyList.NBT));
        LastPrice lp2 = new LastPrice(false, "B", CurrencyList.NBT, new Amount(100, CurrencyList.NBT));
        LastPrice lp3 = new LastPrice(false, "C", CurrencyList.NBT, new Amount(101, CurrencyList.NBT));
        currentPriceList.add(lp);
        currentPriceList.add(lp2);
        currentPriceList.add(lp3);

        LastPrice good = FeedQuality.handleNormal(0.01, new PriceBatch(currentPriceList));
        double q = good.getPrice().getQuantity();
        assertTrue(good.getPrice().getQuantity() == 100.0);

    }

}
