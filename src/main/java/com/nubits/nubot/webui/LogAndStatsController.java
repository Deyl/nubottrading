package com.nubits.nubot.webui;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nubits.nubot.ALPClient.ALPStats;
import com.nubits.nubot.bot.Global;
import com.nubits.nubot.bot.SessionManager;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.Order;
import com.nubits.nubot.models.PairBalance;
import com.nubits.nubot.utils.Utils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;

/**
 * the log endpoints. see definitions in logback.xml and utils.LogVerboseFilter
 */
public class LogAndStatsController {

    final static Logger LOG = LoggerFactory.getLogger(LogAndStatsController.class);


    private String orderEndPoint = "orders";

    private String balanceEndPoint = "balances";

    private String logEndPoint = "logdump";

    private String alpStatsEndpoint = "alpstats";

    private static final int maxSize = 100000;

    public LogAndStatsController() {

        get("/" + logEndPoint, "application/json", (request, response) -> {
            LOG.trace("/logdump called");
            JsonObject object = new JsonObject();

            String f = Global.sessionPath + "/" + Settings.LOG_STANDARD_LIVE_FILENAME;
            if (Global.options.verbosity.equalsIgnoreCase(Settings.LEVEL_HIGH)) {
                f = Global.sessionPath + "/" + Settings.LOG_VERBOSE_LIVE_FILENAME;
            }

            try {
                String l = new String(Files.readAllBytes(Paths.get(f)));

                if (l.length() > maxSize)
                    l = l.substring(l.length() - maxSize, l.length());
                object.addProperty("log", l);
                return object;
            } catch (Exception e) {
                LOG.trace("empty log cue" + e.toString());
                object.addProperty("log", Settings.APP_NAME);
                return object;
            }
        });


        get("/" + orderEndPoint, "application/json", (request, response) -> {
            LOG.trace("/" + orderEndPoint + " called");
            Map opmap = new HashMap();
            int numbuys = 0;
            int numsells = 0;

            if (SessionManager.isModeActive() && Global.orderManager != null) {
                try {

                    numbuys = Global.orderManager.fetchBuyOrdersTimeBound(Settings.ORDER_MAX_INTERVAL);
                    numsells = Global.orderManager.fetchSellOrdersTimeBound(Settings.ORDER_MAX_INTERVAL);
                    Global.orderManager.logActiveOrders();

                    LOG.trace("GET /info : buys: " + numbuys);
                    LOG.trace("GET /info : sells: " + numsells);

                    ArrayList<Order> ol = Global.orderManager.getOrderList();
                    opmap.put("orders", ol);
                    LOG.trace("orders: " + ol);

                } catch (Exception e) {
                    LOG.error(e.toString());
                }
            }

            opmap.put("buys", numbuys);
            opmap.put("sells", numsells);


            String json = new Gson().toJson(opmap);
            LOG.trace("info/ data: " + json);
            return json;
        });


        get("/" + balanceEndPoint, "application/json", (request, response) -> {
            LOG.trace("/" + balanceEndPoint + " called");
            Map opmap = new HashMap();

            if (SessionManager.isSessionRunning() && Global.balanceManager != null) {
                try {
                    try {
                        //query only up to every X msec, otherwise just get the last info
                        //this caps the maximum queries we can do, so to not overload the exchange
                        Global.balanceManager.fetchBalancePairTimeBound(Global.options.getPair(), Settings.BALANCE_MAX_INTERVAL);
                        PairBalance balance = Global.balanceManager.getPairBalance();
                        opmap.put("pegBalance", prepareBalanceObject("peg", balance));
                        opmap.put("nbtBalance", prepareBalanceObject("nbt", balance));
                    } catch (Exception e) {
                        LOG.error(e.toString());
                    }

                } catch (Exception e) {
                    LOG.error(e.toString());
                    LOG.error("" + e.getStackTrace());
                }
            }

            String json = new Gson().toJson(opmap);
            return json;
        });


        get("/" + alpStatsEndpoint, "application/json", (request, response) -> {
            LOG.debug("/" + alpStatsEndpoint + " called"); //TODO remove
            Map opmap = new HashMap();

            if (SessionManager.isSessionRunning() && Global.options.poolModeActive) {
                try {
                    //this caps the maximum queries we can do, so to not overload the exchange
                    ALPStats latestStats = Global.alpStatsManager.getStats();
                    opmap.put("connected", latestStats.isConnected());
                    opmap.put("serverAddress", latestStats.getServerAddress());

                    opmap.put("roundReward", latestStats.getRoundReward());
                    opmap.put("totalReward", latestStats.getTotalReward());
                    opmap.put("poolUserID", latestStats.getPoolUserID());
                    opmap.put("nextPayoutDate", latestStats.getNextPayoutDate());

                    opmap.put("lastPayoutDate", latestStats.getLastPayoutDate());
                    opmap.put("askPayoutRank1", latestStats.getAskPayoutRank1());
                    opmap.put("askPayoutRank2", latestStats.getAskPayoutRank2());

                    opmap.put("bidPayoutRank1", latestStats.getBidPayoutRank1());
                    opmap.put("bidPayoutRank2", latestStats.getBidPayoutRank2());

                    opmap.put("serverUpTime", latestStats.getServerUpTime());

                    opmap.put("rawHTMLfooter", latestStats.getRawHTMLfooter());

                    opmap.put("created", latestStats.getCreated());

                } catch (Exception e) {
                    LOG.error(e.getMessage());
                }
            } else {
                opmap.put("status", "nubot offline or poolmode off");
            }

            String json = new Gson().toJson(opmap);

            return json;
        });

    }

    private HashMap prepareBalanceObject(String type, PairBalance balance) {
        HashMap balanceObj = new JSONObject();

        if (type.equalsIgnoreCase("peg")) {
            balanceObj.put("currencyCode",
                    balance.getPEGAvailableBalance().getCurrency().getCode().toUpperCase());

            balanceObj.put("balanceTotal",
                    Utils.formatNumber(balance.getPEGBalance().getQuantity(), 6));
            balanceObj.put("balanceT1",
                    Utils.formatNumber(balance.getPEGT1().getQuantity(), 6));
            balanceObj.put("balanceT2",
                    Utils.formatNumber(balance.getPEGT2().getQuantity(), 6));

        } else // nbt
        {
            balanceObj.put("currencyCode", balance.getNBTAvailable().getCurrency().getCode().toUpperCase());

            balanceObj.put("balanceTotal",
                    Utils.formatNumber(balance.getNubitsBalance().getQuantity(), 6));
            balanceObj.put("balanceT1",
                    Utils.formatNumber(balance.getNBTT1().getQuantity(), 6));
            balanceObj.put("balanceT2",
                    Utils.formatNumber(balance.getNBTT2().getQuantity(), 6));
        }
        return balanceObj;
    }

}
