package com.nubits.nubot.webui;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.launch.MainLaunch;
import com.nubits.nubot.utils.FilesystemUtils;
import com.nubits.nubot.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Spark;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;


/**
 * the main UI server
 */
public class UiServer {

    final static Logger LOG = LoggerFactory.getLogger(UiServer.class);

    /**
     * start the UI server with a config file
     */
    public static void startUIserver(String configFile, boolean openConfig, boolean alert, String alertMessage, boolean openBrowser) {

        Global.isServer = true;
        //Check if another server is running
        String lockFileStr = FilesystemUtils.getLockFileName(Global.options.webport);
        if (FilesystemUtils.fileExists(lockFileStr)) {
            LOG.error("Another session of the GUI server is already active on port " + Global.options.webport + ". Please stop that before running, or choose a different port. " +
                    "If you keep receiving this message and you are sure there are no other NuBot GUI servers running, " +
                    "try to manually delete the hidden files located under : " + lockFileStr);
            MainLaunch.exitWithNotice("Another server running. Cannot have simultaneous sessions on the same port.");
        } else {
            //Create the lock file
            FilesystemUtils.writeToFile("server running. Start timestamp : " + System.currentTimeMillis(), lockFileStr, true);

            //hook it up for deletion
            File lockFile = new File(lockFileStr);
            lockFile.getParentFile().mkdirs();
            try {
                lockFile.createNewFile();
                lockFile.deleteOnExit();

            } catch (IOException e) {
                LOG.error(e.getMessage());
            }
        }

        LOG.info("start ui server. configfile " + configFile);

        //set up all endpoints

        String urlStr = "http://localhost:" + Global.options.webport;
        LOG.info("launching on " + urlStr);

        Spark.port(Global.options.webport);
        //Set folder for static resources
        Spark.externalStaticFileLocation(FilesystemUtils.getBotAbsolutePath() + "/res/UI");

        //binds GET and POST
        LayoutTemplateEngine tmpl = new LayoutTemplateEngine(Settings.HTML_FOLDER);

        new ConfigController(configFile);

        new LogAndStatsController();

        //controller wants some map
        Map empty = new HashMap();

        get("/", (request, response) -> new ModelAndView(empty, Settings.HTML_FOLDER + "dashboard.mustache"), tmpl);

        Map configmap = new HashMap();
        configmap.put("configfile", configFile);
        get("/configui", (request, response) -> new ModelAndView(configmap, Settings.HTML_FOLDER + "config.mustache"), tmpl);

        get("/pool", (request, response) -> new ModelAndView(empty, Settings.HTML_FOLDER + "pool.mustache"), tmpl);

        get("/docu", (request, response) -> new ModelAndView(empty, Settings.HTML_FOLDER + "docu.mustache"), tmpl);

        get("/disclaimer", (request, response) -> new ModelAndView(empty, Settings.HTML_FOLDER + "disclaimer.mustache"), tmpl);

        new BotController();

        LOG.debug("Opening the system default browser :");
        if (openConfig)
            urlStr += "/configui"; //if the user didn't specify cfg, it will automatically go to /configui
        if (alert) {
            try {
                urlStr += "?alert=true&alertmsg=" + URLEncoder.encode(alertMessage, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                LOG.error(e.toString());
            }
        }
        if (openBrowser) {
            Utils.launchBrowser(urlStr);
        }

    }


}