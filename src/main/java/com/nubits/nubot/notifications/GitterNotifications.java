/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.notifications;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.bot.SessionManager;
import com.nubits.nubot.global.CredentialManager;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.URL;

/**
 *
 */
public class GitterNotifications {
    private static final Logger LOG = LoggerFactory.getLogger(GitterNotifications.class.getName());


    public static void sendMessage(String message) {
        sendMessageImpl(Global.credentialManager.get(CredentialManager.GITTER_NOTIFICATIONS_ROOM_ID), message);

    }

    public static void sendMessageCritical(String message) {
        sendMessageImpl(Global.credentialManager.get(CredentialManager.GITTER_CRITICAL_ROOM_ID), message);
    }


    private static String sendMessageImpl(String roomID, String text) {
        String answer = null;
        String url = "https://api.gitter.im/v1/rooms/" + roomID + "/chatMessages";

        String sessionId = "";
        if (SessionManager.getSessionId() != null) {
            sessionId = SessionManager.getSessionId();
        }

        String publicAddress = "";


        publicAddress = Global.options.getNubitAddress();


        text = Global.options.poolModeActive ? text + " *[" + sessionId + " - pool: " + Global.options.poolURI + " - " + publicAddress + "] *" : text + " *[" + sessionId + " - " + publicAddress + "] *";

        JSONObject json = new JSONObject();
        json.put("text", text);
        String postMessage = json.toString();

        // add header
        Header[] headers = new Header[3];
        headers[0] = new BasicHeader("Authorization", "Bearer " + Global.credentialManager.get(CredentialManager.GITTER_ACCESS_TOKEN));
        headers[1] = new BasicHeader("Accept", "application/json");
        headers[2] = new BasicHeader("Content-type", "application/json");

        URL queryUrl;
        try {
            queryUrl = new URL(url);
        } catch (MalformedURLException ex) {
            LOG.error(ex.toString());
            return null;
        }
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = null;
        HttpResponse response = null;

        try {
            post = new HttpPost(url);
            post.setEntity(new ByteArrayEntity(postMessage.toString().getBytes("UTF8")));
            post.setHeaders(headers);
            response = client.execute(post);

        } catch (NoRouteToHostException e) {

            post.abort();

            LOG.error(e.toString());
            return null;
        } catch (SocketException e) {
            post.abort();
            LOG.error(e.toString());
            return null;
        } catch (Exception e) {
            post.abort();
            LOG.error(e.toString());
            return null;
        }
        BufferedReader rd;
        try {
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));


            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                buffer.append(line);
            }

            answer = buffer.toString();
        } catch (IOException ex) {

            LOG.error(ex.toString());
            return null;
        } catch (IllegalStateException ex) {

            LOG.error(ex.toString());
            return null;
        }


        LOG.trace("\nSending POST request to URL : " + url);
        LOG.trace("Post parameters : " + post.getEntity());
        LOG.trace("Response Code : " + response.getStatusLine().getStatusCode());
        LOG.trace("Response :" + response);

        LOG.trace("Answer :" + answer);

        return answer;
    }
}
