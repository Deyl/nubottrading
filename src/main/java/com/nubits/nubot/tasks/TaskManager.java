/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.tasks;

import com.nubits.nubot.RPC.NudClient;
import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.strategy.Primary.StrategyPrimaryPegTask;
import com.nubits.nubot.strategy.Secondary.StrategySecondaryPegTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


public class TaskManager {

    private static final Logger LOG = LoggerFactory.getLogger(TaskManager.class.getName());
    private static final String STRATEGY_FIAT = "Strategy Fiat Task";
    private static final String STRATEGY_SECONDARY = "Strategy Secondary Task";

    private BotTask strategyFiatTask;
    private BotTask checkOrdersTask;
    private BotTask checkNudTask;
    private BotTask ALPTask;


    //these are used for secondary peg strategy
    private BotTask secondaryPegTask;
    private BotTask priceTriggerTask;

    private ArrayList<BotTask> taskList;
    private boolean running;
    private boolean initialized = false;

    public TaskManager() {
        this.running = false;
        taskList = new ArrayList<BotTask>();

        //assign default values just for testing without Global.options loaded
    }

    /**
     * setup the task for checking Nu RPC
     */
    public void setupNuRPCTask() {
        LOG.info("Setting up RPC client on " + Global.options.getNudIp() + ":" + Global.options.getNudPort());

        Global.rpcClient = new NudClient(Global.options.getNudIp(), Global.options.getNudPort(),
                Global.options.getRpcUser(), Global.options.getRpcPass(), true,
                Global.options.getNubitAddress(), Global.options.getPair(), Global.options.getExchangeName());

        this.setNudTask();

    }

    public void startTaskNu() {
        LOG.info("Starting task : Check connection with Nud");
        this.getCheckNudTask().start();
    }


    public void setNudTask() {
        this.checkNudTask = new BotTask(
                new CheckNudTask(), Settings.CHECK_NUD_INTERVAL, "checkNud");
        taskList.add(checkNudTask);

    }

    public void setTasks() {
        //connectivity tasks

        LOG.debug("setting up tasks");

        checkOrdersTask = new BotTask(
                new CheckOrdersTask(), Settings.CHECK_ORDERS_SECONDS, "checkOrders");
        taskList.add(checkOrdersTask);


        strategyFiatTask = new BotTask(
                new StrategyPrimaryPegTask(), Settings.EXECUTE_STRATEGY_INTERVAL, STRATEGY_FIAT);
        taskList.add(strategyFiatTask);

        secondaryPegTask = new BotTask(
                new StrategySecondaryPegTask(), Settings.EXECUTE_STRATEGY_INTERVAL, STRATEGY_SECONDARY);
        taskList.add(secondaryPegTask);

        //Select the correct interval
        int checkPriceInterval = Settings.CHECK_PRICE_INTERVAL;
        CurrencyPair pair = Global.options.getPair();
        boolean checkFiat = pair.getPaymentCurrency().isFiat() && !Global.swappedPair
                || pair.getOrderCurrency().isFiat() && Global.swappedPair;

        if (checkFiat) {
            checkPriceInterval = Settings.CHECK_PRICE_INTERVAL_FIAT;
        }

        priceTriggerTask = new BotTask(
                new PriceMonitorTriggerTask(), checkPriceInterval, "priceTriggerTask");
        taskList.add(priceTriggerTask);


        if (Global.options.poolModeActive) {
            ALPTask = new BotTask(new ALPTask(), Global.options.poolSubmitInterval, "ALPTask");
            taskList.add(ALPTask);
        }

        initialized = true;
    }

    public void startAll() {
        for (int i = 0; i < taskList.size(); i++) {
            BotTask task = taskList.get(i);
            task.start();
        }

    }

    public boolean stopAll() throws IllegalStateException {
        LOG.info("Stopping all BotTasks. ");
        boolean sentNotification = false;
        for (int i = 0; i < taskList.size(); i++) {

            BotTask bt = taskList.get(i);

            LOG.debug("Shutting down " + bt.getName() + " (" + bt.getTask().getClass().getSimpleName() + ")");
            try {
                bt.stop();
            } catch (IllegalStateException e) {
                LOG.error("" + e);
                throw e;
            }
        }
        LOG.info("BotTasks stopped. ");
        return true;
    }

    public void printTasksStatus() {
        for (int i = 0; i < taskList.size(); i++) {
            BotTask task = taskList.get(i);
            LOG.info("Task name : " + task.getName() + ""
                    + " running : " + task.isRunning());
        }
    }


    /**
     * @return the isRunning
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * @param isRunning the isRunning to set
     */
    public void setRunning(boolean isRunning) {
        this.running = isRunning;
    }

    public BotTask getStrategyFiatTask() {
        return strategyFiatTask;
    }

    public BotTask getCheckOrdersTask() {
        return checkOrdersTask;
    }

    public BotTask getSecondaryPegTask() {
        return secondaryPegTask;
    }

    public BotTask getPriceTriggerTask() {
        return priceTriggerTask;
    }

    public BotTask getCheckNudTask() {
        return checkNudTask;
    }

    public BotTask getALPTask() {
        return ALPTask;
    }

    public void setALPTask(BotTask ALPTask) {
        this.ALPTask = ALPTask;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public ArrayList<BotTask> getTaskList() {
        return taskList;
    }

    public void setTaskList(ArrayList<BotTask> taskList) {
        this.taskList = taskList;
    }
}
