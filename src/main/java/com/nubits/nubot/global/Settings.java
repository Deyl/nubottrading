/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.global;

/**
 * This class contains a list of settings that can be configured by developers
 * Settings are grouped and commented.
 */

public class Settings {

    /**
     * Misc
     */
    public final static String APP_NAME = "NuBot";

    /**
     * the precision used to round
     */
    public static final int DEFAULT_PRECISION = 8;

    /**
     * Force the a spread to avoid collisions (multi-custodians)
     */
    public static final double FORCED_OFFSET_TOTAL = 0.01; //expressed in USD, both sides

    // ------ Timing ------
    /**
     * [seconds] Check orders, log, and submit liquidityinfo
     */
    public static final int CHECK_ORDERS_SECONDS = 130;

    /**
     * [seconds] Execute StrategyTask
     */
    public static final int EXECUTE_STRATEGY_INTERVAL = 41;


    /**
     * [seconds] Check connection with nudaemon
     */
    public static final int CHECK_NUD_INTERVAL = 30;

    /**
     * [seconds]
     */
    public static final int CHECK_PRICE_INTERVAL = 61;

    /**
     * [seconds] ~ 8 hours, used for fiat only
     */
    public static final int CHECK_PRICE_INTERVAL_FIAT = 8 * 60 * 59 * 1000;

    /**
     * [minutes] Used in multi-custodian mode
     */
    public static final int RESET_EVERY_MINUTES = 3;

    /**
     * Timeout for NTP calls
     */
    public static final int NTP_TIMEOUT = 10 * 1000;

    // ------ Paths and filenames
    // Refer to md/FILES-AND-FOLDERS.md for the convention of where to place files and folders
    // In defining folder names, omit the "/" at the end


    /**
     * the file for configuring logging during test. uses sift to direct tests to different files
     */
    public static final String STREAM_LOGXML = "config/logging/stream_logback.xml";

    /**
     * foldername containing log messages belonging to a session
     * the relative path for resources
     */
    public final static String RES_PATH = "res";

    /**
     * Path to auth file
     */
    public static final String PATH_TO_AUTH_FILE = RES_PATH + "/authkey.txt"; // (-) Remember to change it in build.gradle too

    public final static String KEYSTORE_PATH = RES_PATH + "/" + "ssl/nubot_keystore.jks";

    public final static String HIDDEN_USERDIR_NAME = ".nubot"; //Name of the directory used for embedding files

    /**
     * folder containing resources needed at runtime
     */
    public final static String FROZEN_FUNDS_PATH = RES_PATH + "/" + "frozen-funds/";

    public final static String DEFAULT_CONFIG_FILENAME = "nubot-config.json";//Used by UI in case file not explicitly declared

    public final static String CONFIG_DIR = "config";

    public final static String DEFAULT_CONFIG_FILE_PATH = CONFIG_DIR + "/" + DEFAULT_CONFIG_FILENAME;

    public final static String IMAGE_FOLDER = RES_PATH + "/" + "images";

    public final static String SESSION_LOG = "session_";

    public final static String ORDERS_FILENAME = "orders_history"; //Filename for historical snapshot of active orders

    public final static String BALANCES_FILEAME = "balance_history"; //Filename for historical snapshot of balance

    public final static String WALLSHIFTS_FILENAME = "wall_shifts"; //Filename for recording wallshifts

    public final static String EXECUTED_TRADES = "executed_trades.json"; //Filename for recording executeTrades

    public final static String TESTS_LOG_PREFIX = "tests"; //Prefix used in naming the directory for saving the output of tests

    public static final String TESTS_CONFIG_PATH = CONFIG_DIR + "/" + "myconfig"; //Directory containing configuration files used in tests

    public final static String CURRENCY_FILE_PATH = RES_PATH + "/" + "currencies.csv";


    //--- Logging ---

    /**
     * the main logging file
     */
    public static final String LOGXML = "config/logging/logback.xml";

    /**
     * the file for configuring logging during test. uses sift to direct tests to different files
     */
    public static final String TEST_LOGXML = "config/logging/test_logback.xml";

    public static final String LOG_VERBOSE_FILENAME = "verbose.log";   //Must be the same as defined in logback.xml files
    public static final String LOG_STANDARD_FILENAME = "standard.log";   //Must be the same as defined in logback.xml files
    public static final String LOG_VERBOSE_LIVE_FILENAME = "verbose_live";   //Must be the same as defined in logback.xml files
    public static final String LOG_STANDARD_LIVE_FILENAME = "standard_live";   //Must be the same as defined in logback.xml files
    /**
     * main logs file. also defined in logback.xml
     */
    public final static String LOGS_PATH = "logs";

    /**
     * testlog directory. also defined in test_logback.xml
     */
    public static final String TEST_LOGFOLDER = LOGS_PATH + "/" + "tests";

    /**
     * foldername containing log messages not belonging to any particular log session
     */
    public static final String ORPHANS_LOGFOLDER = LOGS_PATH + "/" + "idles";

    /**
     * foldername containing log messages belonging to a session
     */
    public static final String SESSIONS_LOGFOLDER = LOGS_PATH + "/" + "bot_sessions";


    public final static String SESSION_LOGGER_NAME = "SessionLOG";
    public static final int DELAY_CONN = 1;
    public final static int DELAY_ORDERCHECK = 40;

    public final static long TIMEOUT_QUERY_RETRY = 10 * 1000; // For how long should the wrapper retry the call if the exchange has found busy
    public final static long RETRY_SLEEP_INCREMENT = 300; // Sleep time increment each new cycle of retry. Cannot be less than 100 ms


    /**
     * a utility file which gets created in the distribution folder
     */
    public static String INFO_FILE = "res/.info";

    public static String HTML_FOLDER = "." + "/res" + "/" + "UI" + "/" + "templates/";

    /**
     * for OrderManager: the maximum refresh period triggered from the UI. strategy can trigger at higher rates
     */
    public static int ORDER_MAX_INTERVAL = 12 * 1000;

    /**
     * for BalanceManager: the maximum refresh period triggered from the UI. strategy can trigger at higher rates
     */
    public static int BALANCE_MAX_INTERVAL = 12 * 1000;

    public static final String OFFICIAL_REPOSITORY_URL = "https://bitbucket.org/JordanLeePeershares/nubottrading";

    public static final String OFFICIAL_DOWNLOADS_URL = "https://bitbucket.org/JordanLeePeershares/nubottrading/downloads";

    public static final String SUBSCRIBER_PROP_FILE = CONFIG_DIR + "/" + "subscriber.properties";

    public static final String STREAMER_PROP_FILE = CONFIG_DIR + "/" + "streamer.properties";

    public static final String LEVEL_HIGH = "high";

    public static final String LEVEL_NORMAL = "normal";

    public static final String LEVEL_LOW = "low";

    public static final long FEEDFETCH_DEFAULT_INTERVAL = 50 * 1000; //used for crypto in general

    public static final long FEEDFETCH_FIAT_INTERVAL = 2 * 60 * 60 * 1000; //2 hours

    /**
     * Minimum number of backup feeds accepted
     */
    public static final int MIN_NUMBER_OF_BKUP_FEEDS = 2;

    public static final int PRICE_DISTANCE_MAX_PERCENT = 10; //this is the maximum distance in percentage between two prices to consider them good

    public static final String DEFAULT_STREAMING_SERVER = "stream.tradingbot.nu:8889"; //Clarify with inuit

    public static final double DEFAULT_WALLSHIFT_TRESHOLD = 0.15; //expressed in absolute percentage

    public static final double MAX_DEVIANCE_REMOTEFEED = 1; //expressed in absolute percentage

    public static final int WEB_DEFAULT_PORT = 8889; //port for web server

    public static final String REQUEST_TOKEN_FORM = "http://goo.gl/forms/nPjiDdsIUb";

    public static int MAX_NUMBER_OF_ORDERS_PER_SIDE = 12; //For liquidity tier2 distribution, excluding the tier1 wall

    /**
     * Min and max value admitted for spread
     * https://discuss.nubits.com/t/passed-motion-to-regulate-spread-values-for-liquidity-operations/2207
     */
    public static double MAX_SPREAD = 0.052; //USD

    public static double MIN_SPREAD = 0.007;  //USD

    public static final double DEFAULT_OFFSET_HIGHVOLATILITY = 0.025; //Expressed in $

    public static final double DEFAULT_OFFSET_LOWVOLATILITY = 0.01; //Expressed in $

    public static final double DEFAULT_OFFSET_NOVOLATILITY = 0; //Expressed in $

    /**
     * Replenish Tier1 wall
     */

    public static final double T1LIQUIDITY_PERCENTAGE_THRESHOLD = 0.1; //0.1 = 10%

    public static final double ALLOW_T1LIQUIDITY_UNDERTRESHOLD_MINUES_HIGHRISK = 4; //minutes
    public static final double ALLOW_T1LIQUIDITY_UNDERTRESHOLD_MINUES_MIDHRISK = 15; //minutes
    public static final double ALLOW_T1LIQUIDITY_UNDERTRESHOLD_MINUES_LOWRISK = 30; //minutes

    /**
     * Pool related settings
     */
    public static final int SUBMIT_POOL_MIN_INTERVAL = 40; //Minimum interval allowed in seconds
    public static final long ALP_SLEEP_AFTER_PREPARE_HTTPREQUEST = 2 * 1000;//interval in ms to sleep and give ALP time . prevent breaking logic

}
