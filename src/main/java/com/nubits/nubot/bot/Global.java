/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.bot;

import com.nubits.nubot.ALPClient.ALPService;
import com.nubits.nubot.ALPClient.ALPStats;
import com.nubits.nubot.ALPClient.ALPStatsManager;
import com.nubits.nubot.RPC.NudClient;
import com.nubits.nubot.exchanges.ExchangeLiveData;
import com.nubits.nubot.global.CredentialManager;
import com.nubits.nubot.launch.ShutDownProcess;
import com.nubits.nubot.options.NuBotOptions;
import com.nubits.nubot.strategy.BalanceManager;
import com.nubits.nubot.strategy.OrderManager;
import com.nubits.nubot.streamclient.Subscriber;
import com.nubits.nubot.tasks.TaskManager;
import com.nubits.nubot.trading.LiquidityDistribution.LiquidityCurve;
import com.nubits.nubot.trading.LiquidityDistribution.LiquidityDistributionModel;
import com.nubits.nubot.trading.LiquidityDistribution.ModelParameters;
import com.nubits.nubot.trading.TradeInterface;
import com.nubits.nubot.utils.FrozenBalancesManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Global object for NuBot
 */
public class Global {

    final static Logger LOG = LoggerFactory.getLogger(Global.class);

    public static boolean isServer = false;

    public static boolean isSubscribed = false;

    public static int webPort = -1; //if port is set via CLI, this value will be used

    public static Thread mainThread;

    public static NuBotOptions options;

    /**
     * the bot connected to the global thread
     */
    public static NuBotBase bot;

    public static OrderManager orderManager;

    public static ALPService alpManager;

    public static ALPStatsManager alpStatsManager;

    public static BalanceManager balanceManager;

    public static LiquidityDistributionModel ldm;

    public static TradeInterface trade;

    public static ExchangeLiveData exchangeLiveData;

    public static TaskManager taskManager;

    public static NudClient rpcClient;

    public static double conversion = 1; //Change this? update SendLiquidityinfoTask

    public static FrozenBalancesManager frozenBalancesManager;

    public static boolean swappedPair; //true if payment currency is NBT

    public static String sessionPath; //Path used for each session
    public static String logFolder; //Global path used once

    public static String currentOptionsFile;

    public static boolean isSimulation = false;

    public static CredentialManager credentialManager;

    public static Subscriber subscriber;

    public static String sellWallOrderID = "";
    public static double sellWallOrderSize = 0;

    public static String buyWallOrderID = "";
    public static double buyWallOrderSize = 0;

    /**
     * process shutdown mechanics
     */
    public static void createShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(new ShutDownProcess()));
    }

    public static void defineLogPath(String path) {
        path = path.replaceAll(":", "_");
        Global.sessionPath = path;
        MDC.put("session", Global.sessionPath);
        LOG.info("defined session log path " + Global.sessionPath);
    }

    /**
     * set LiquidityDistributionManager in global
     */
    public static void updateLiquidityDistManger() {

        LOG.debug("updating setting liquidity distribution manager ");

        ModelParameters sellParams = new ModelParameters(Global.options.bookSellOffset, Global.options.bookSellwall,
                Global.options.bookSellInterval, LiquidityCurve.generateCurve(Global.options.bookSellType,
                Global.options.bookSellSteepness), Global.options.bookSellMaxVolumeCumulative);
        ModelParameters buyParams = new ModelParameters(Global.options.bookBuyOffset, Global.options.bookBuywall,
                Global.options.bookBuyInterval, LiquidityCurve.generateCurve(Global.options.bookBuyType,
                Global.options.bookBuySteepness), Global.options.bookBuyMaxVolumeCumulative);


        Global.ldm = new LiquidityDistributionModel(sellParams, buyParams);

    }


}
