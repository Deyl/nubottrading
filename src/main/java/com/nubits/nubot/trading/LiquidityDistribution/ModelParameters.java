/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.trading.LiquidityDistribution;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModelParameters {

    private static final Logger LOG = LoggerFactory.getLogger(ModelParameters.class.getName());
    private double offset; //Distance from 1$ target price, expressed in USD
    private double wallHeight; //amount of base-liquidity available at the best price, expressed in USD
    private double interval; //distance between orders, expressed in USD
    private double cap; //Maximum cumulative liquidity to put on order, expressed in USD
    private LiquidityCurve curve; //type of model

    public ModelParameters(double offset, double wallHeight, double interval, LiquidityCurve curve, double cap) {
        this.offset = offset;
        this.wallHeight = wallHeight;
        this.interval = interval;
        this.curve = curve;
        this.cap = cap;
    }

    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    public double getWallHeight() {
        return wallHeight;
    }

    public void setWallHeight(double wallHeight) {
        this.wallHeight = wallHeight;
    }

    public double getInterval() {
        return interval;
    }

    public void setInterval(double interval) {
        this.interval = interval;
    }

    public LiquidityCurve getCurve() {
        return curve;
    }

    public void setCurve(LiquidityCurve curve) {
        this.curve = curve;
    }

    public double getCap() {
        return cap;
    }

    public void setCap(double cap) {
        this.cap = cap;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ModelParameters that = (ModelParameters) o;

        if (Double.compare(that.offset, offset) != 0) return false;
        if (Double.compare(that.wallHeight, wallHeight) != 0) return false;
        if (Double.compare(that.interval, interval) != 0) return false;
        if (Double.compare(that.cap, cap) != 0) return false;
        return !(curve != null ? !curve.equals(that.curve) : that.curve != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(offset);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(wallHeight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(interval);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(cap);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (curve != null ? curve.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ModelParameters{" +
                "offset=" + offset +
                ", wallHeight=" + wallHeight +
                ", interval=" + interval +
                ", cap=" + cap +
                ", curve=" + curve +
                '}';
    }
}
