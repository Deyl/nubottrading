/*
 * Copyright (c) 2015. Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.trading.wrappers;

import com.nubits.nubot.ALPClient.ErrorGeneratingALPRequestException;
import com.nubits.nubot.exchanges.ExchangeFacade;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.*;
import com.nubits.nubot.models.Currency;
import com.nubits.nubot.trading.*;
import com.nubits.nubot.trading.keys.ApiKeys;
import com.nubits.nubot.utils.Utils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class CryptsyWrapper implements TradeInterface {

    private static final Logger LOG = LoggerFactory.getLogger(CryptsyWrapper.class.getName());
    //Class fields
    private ApiKeys keys;
    protected CryptsyService service;
    public final String EXCHANGE_NAME = ExchangeFacade.CRYPTSY;
    private final int TIME_OUT = 15000;
    private String apiBaseUrl;
    private String checkConnectionUrl = "https://www.cryptsy.com/";
    private final String SIGN_HASH_FUNCTION = "HmacSHA512";
    private final String ENCODING = "UTF-8";

    private HashMap<String, String> marketMap;

    //handle concurrent requests
    private boolean isBusy;

    //Entry Points
    private final String API_BASE_URL = "https://api.cryptsy.com/api";
    private final String API_GET_INFO = "getinfo";
    private final String API_CREATE_ORDER = "createorder";
    private final String API_GETMARKETS = "getmarkets";
    private final String API_GETORDERS = "myorders";
    private final String API_GETORDERS_ALL = "allmyorders";
    private final String API_CLEAR_ORDERS = "cancelmarketorders";
    private final String API_DELETE_ORDER = "cancelorder";
    private final String API_GET_ORDER = "getorderstatus";
    private final String API_GET_TRADES = "allmytrades";
    private final String API_GET_ORDERBOOK = "marketorders";


    //Tokens
    private final String TOKEN_BAD_RETURN = "No Connection With Exchange";


    //Pool queue
    protected long lastRequest = 0;
    //Errors
    ErrorManager errors = new ErrorManager();

    public CryptsyWrapper(ApiKeys keys) {
        this.keys = keys;
        service = new CryptsyService(keys);
        marketMap = new HashMap<>();
        setupErrors();
        this.setFree();
    }

    public boolean isBusy() {
        return isBusy;
    }

    public boolean isFree() {
        return !isBusy;
    }

    public void setBusy() {
        this.isBusy = true;
    }

    public void setFree() {
        this.isBusy = false;
    }

    private void setupErrors() {
        errors.setExchangeName(this.EXCHANGE_NAME);
    }

    private ApiResponse getQuery(String url, String method, HashMap<String, String> query_args, boolean needAuth, boolean isGet) {
        ApiResponse apiResponse = new ApiResponse();
        String queryResult = query(url, method, query_args, needAuth, isGet);
        if (queryResult == null) {
            apiResponse.setError(errors.nullReturnError);
            return apiResponse;
        }
        if (queryResult.equals(TOKEN_BAD_RETURN)) {
            apiResponse.setError(errors.noConnectionError);
            return apiResponse;
        }

        JSONParser parser = new JSONParser();

        try {
            JSONObject httpAnswerJson = (JSONObject) (parser.parse(queryResult));
            String success = (String) httpAnswerJson.get("success");
            if (success.equals("0")) {
                ApiError error = errors.apiReturnError;
                error.setDescription(httpAnswerJson.get("error").toString());
                apiResponse.setError(error);
                return apiResponse;
            }
            apiResponse.setResponseObject(httpAnswerJson);
        } catch (ClassCastException cce) {
            //if casting to a JSON object failed, try a JSON Array
            try {
                JSONArray httpAnswerJson = (JSONArray) (parser.parse(queryResult));
                apiResponse.setResponseObject(httpAnswerJson);
            } catch (ParseException pe) {
                LOG.error("httpResponse: " + queryResult + " \n" + pe.toString());
                apiResponse.setError(errors.parseError);
            }
        } catch (ParseException pe) {
            LOG.error("httpResponse: " + queryResult + " \n" + pe.toString());
            apiResponse.setError(errors.parseError);
            return apiResponse;
        }
        return apiResponse;
    }


    @Override
    public ApiResponse getAvailableBalances(CurrencyPair pair) {
        return getBalanceImpl(pair, null);
    }

    @Override
    public ApiResponse getAvailableBalance(Currency currency) {
        return getBalanceImpl(null, currency);
    }

    public ApiResponse getBalanceImpl(CurrencyPair pair, Currency currency) {
        ApiResponse apiResponse = new ApiResponse();
        String base = API_BASE_URL;
        String method = API_GET_INFO;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();
        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
             /*
                {"success":"1","return":{"balances_available":{"007":"0.00000000","42":"0.00000000","8BIT":"0.00000000",    "ACOIN":"0.00000000","ZEIT":"0.00000000","ZET":"0.00000000","ZRC":"0.00000000"},"servertimestamp":1448295160,"servertimezone":"EST","serverdatetime":"2015-11-23 11:12:40","openordercount":0}}
                 */

            JSONObject httpAnswerJsonSimple = (JSONObject) response.getResponseObject();
            String strObj = httpAnswerJsonSimple.toJSONString();

            org.json.JSONObject httpAnswerJson = new org.json.JSONObject(strObj);

            org.json.JSONObject dataJson = (org.json.JSONObject) httpAnswerJson.get("return");
            org.json.JSONObject balancesAvailableJSON = (org.json.JSONObject) dataJson.get("balances_available");

            String namesAvailBalances[] = org.json.JSONObject.getNames(balancesAvailableJSON);


            if (currency == null) { //Get all balances

                boolean foundNBTavail = false;
                boolean foundPEGavail = false;
                boolean foundNBTlocked = false;
                boolean foundPEGlocked = false;
                Amount NBTAvail = new Amount(0, pair.getOrderCurrency()),
                        PEGAvail = new Amount(0, pair.getPaymentCurrency());

                Amount PEGonOrder = new Amount(0, pair.getPaymentCurrency());
                Amount NBTonOrder = new Amount(0, pair.getOrderCurrency());
                String NBTcode = pair.getOrderCurrency().getCode().toUpperCase();
                String PEGcode = pair.getPaymentCurrency().getCode().toUpperCase();


                //Iterate on available balances
                for (int i = 0; i < namesAvailBalances.length; i++) {
                    double tempBalance = balancesAvailableJSON.getDouble(namesAvailBalances[i]);
                    if (namesAvailBalances[i].equals(NBTcode)) {
                        foundNBTavail = true;
                        NBTAvail.setQuantity(tempBalance);
                    } else if (namesAvailBalances[i].equals(PEGcode)) {
                        foundPEGavail = true;
                        PEGAvail.setQuantity(tempBalance);
                    }
                    if (foundNBTavail && foundPEGavail) break;
                }


                //Iterate on locked balances, if any
                if (dataJson.has("balances_hold")) {
                    org.json.JSONObject balancesLocked = (org.json.JSONObject) dataJson.get("balances_hold");
                    String namesLockedBalances[] = org.json.JSONObject.getNames(balancesLocked);
                    for (int i = 0; i < namesLockedBalances.length; i++) {
                        double tempBalance = balancesLocked.getDouble(namesLockedBalances[i]);
                        if (namesLockedBalances[i].equals(NBTcode)) {
                            NBTonOrder.setQuantity(tempBalance);
                        } else if (namesLockedBalances[i].equals(PEGcode)) {
                            PEGonOrder.setQuantity(tempBalance);
                        }
                        if (foundNBTlocked && foundPEGlocked) break;
                    }
                }

                PairBalance balance = new PairBalance(PEGAvail, NBTAvail, PEGonOrder, NBTonOrder);
                apiResponse.setResponseObject(balance);

                if (!(foundNBTavail && foundPEGavail)) {
                    LOG.info("Cannot find a balance for currency with code "
                            + "" + NBTcode + " or " + PEGcode + " in your balance. "
                            + "NuBot assumes that balance is 0");

                }
            } else {
                //Get specific balance
                boolean found = false;
                Amount avail = new Amount(0, currency);
                String code = currency.getCode().toUpperCase();
                for (int i = 0; i < namesAvailBalances.length; i++) {
                    double tempBalance = balancesAvailableJSON.getDouble(namesAvailBalances[i]);
                    if (namesAvailBalances[i].equals(code)) {
                        found = true;
                        avail.setQuantity(tempBalance);
                    }
                    if (found) break;
                }

                if (found) {
                    apiResponse.setResponseObject(avail);
                } else {
                    LOG.warn("Cannot find a balance for currency with code "
                            + code + " in your balance. NuBot assumes that balance is 0");
                    avail.setQuantity(0);
                    apiResponse.setResponseObject(avail);

                }
            }

        } else {
            apiResponse = response;
        }

        return apiResponse;

    }

    @Override
    public ApiResponse getLastPrice(CurrencyPair pair) {
        ApiResponse apiResponse = new ApiResponse();

        try {
            String tickerURL = getTickerUrl(pair);
            String resp = Utils.getHTML(tickerURL, false);
            JSONParser parser = new JSONParser();
            JSONObject responseJSON = (JSONObject) parser.parse(resp);

            JSONObject returnJSON = (JSONObject) (responseJSON.get("return"));
            JSONObject marketsJSON = (JSONObject) (returnJSON.get("markets"));
            JSONObject marketJSON = (JSONObject) (marketsJSON.get(pair.getOrderCurrency().getCode().toUpperCase()));

            double last = Utils.getDouble(marketJSON.get("lasttradeprice"));

            Ticker t = new Ticker(last, last, last);
            LOG.debug("Warning : Cryptsy doesn't have a proper ticker, only last executed trade value is used");

            apiResponse.setResponseObject(t);

        } catch (CryptsyMarketNotFoundException e) {
            apiResponse.setError(new ApiError(-1, e.getMessage()));
        } catch (IOException e) {
            apiResponse.setError(new ApiError(-1, e.getMessage()));
        } catch (ParseException e) {
            apiResponse.setError(new ApiError(-1, e.getMessage()));
        }

        return apiResponse;
    }

    @Override
    public ApiResponse sell(CurrencyPair pair, double amount, double rate) {
        return enterOrderImplementation(Constant.SELL, pair, amount, rate);
    }

    @Override
    public ApiResponse buy(CurrencyPair pair, double amount, double rate) {
        return enterOrderImplementation(Constant.BUY, pair, amount, rate);
    }


    public ApiResponse enterOrderImplementation(String type, CurrencyPair pair, double amount, double rate) {
        ApiResponse apiResponse = new ApiResponse();
        String base = API_BASE_URL;
        String method = API_CREATE_ORDER;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();
        String marketid = "";
        try {
            marketid = getMarketID(pair);
        } catch (CryptsyMarketNotFoundException e) {
            apiResponse.setError(new ApiError(-1, "Can't find a valid marketid for " + pair.toString() + " :" + e.toString()));
            return apiResponse;
        }

        query_args.put("marketid", marketid);
        query_args.put("ordertype", type);
        query_args.put("quantity", Utils.formatNumber(amount, Settings.DEFAULT_PRECISION));
        query_args.put("price", Utils.formatNumber(rate, Settings.DEFAULT_PRECISION));

        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            JSONObject httpAnswerJsonSimple = (JSONObject) response.getResponseObject();
            String orderID = (String) httpAnswerJsonSimple.get("orderid");
            apiResponse.setResponseObject(orderID);
        } else {
            LOG.error("Can't place order " + type + " " + amount + " @ " + rate);
            apiResponse = response;
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getActiveOrders() {
        return getActiveOrders(null);
    }

    @Override
    public ApiResponse getActiveOrders(CurrencyPair pair) {
        return getOrdersImpl(pair, null);
    }

    private ApiResponse getOrdersImpl(CurrencyPair pair, String orderID) {
        ApiResponse apiResponse = new ApiResponse();
        ArrayList<Order> orderList = new ArrayList<Order>();

        String base = API_BASE_URL;

        String method = pair == null ? API_GETORDERS_ALL : API_GETORDERS;

        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();

        if (pair != null) {
            String marketid = "";
            try {
                marketid = getMarketID(pair);
            } catch (CryptsyMarketNotFoundException e) {
                apiResponse.setError(new ApiError(-1, "Can't find a valid marketid for " + pair.toString() + " :" + e.toString()));
                return apiResponse;
            }

            query_args.put("marketid", marketid);
        }
        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            JSONObject httpAnswerJsonSimple = (JSONObject) response.getResponseObject();
            JSONArray ordersArray = (JSONArray) httpAnswerJsonSimple.get("return");
            boolean foundOrder = false;
            Order orderToFind = null;
            for (int i = 0; i < ordersArray.size(); i++) {
                JSONObject orderObject = (JSONObject) ordersArray.get(i);
                Order tempOrder = parseOrder(orderObject, pair);
                if (orderID != null) {
                    //Seeking for a specific order
                    if (tempOrder.getId().equals(orderID)) {
                        orderToFind = tempOrder;
                        foundOrder = true;
                        break;
                    }
                }
                orderList.add(tempOrder);
            }
            if (orderID != null) {
                if (foundOrder) {
                    apiResponse.setResponseObject(orderToFind);
                } else {
                    apiResponse.setError(new ApiError(-1, "Order " + orderID + " not found"));
                }
                return apiResponse;

            } else {

                apiResponse.setResponseObject(orderList);
                return apiResponse;
            }
        } else {
            apiResponse = response;
        }
        return apiResponse;
    }

    private Order parseOrder(JSONObject orderObject, CurrencyPair pair) {

        /*
            {
                marketid -> "344" //Only when pair==null

                "total" -> "10.00000000"
                "quantity" -> "1.00000000"
                "orderid" -> "448995708"
                "created" -> "2015-11-24 10:16:25"
                "price" -> "10.00000000"
                "orig_quantity" -> "1.00000000"
                "ordertype" -> "Sell"
              }
            */
        Order order = new Order();

        order.setType(((String) orderObject.get("ordertype")).toUpperCase());
        order.setId((String) orderObject.get("orderid"));

        if (pair == null) {
            try {
                pair = getPairFromMarketID((String) orderObject.get("marketid"));
            } catch (CryptsyMarketNotFoundException e) {
                LOG.error(e.getMessage());
                pair = CurrencyList.UNKNOWPAIR;
            }
        }
        order.setAmount(new Amount(Utils.getDouble(orderObject.get("quantity")), pair.getOrderCurrency()));
        order.setPrice(new Amount(Utils.getDouble(orderObject.get("price")), pair.getPaymentCurrency()));

        order.setCompleted(false);


        if (pair != null) {
            order.setPair(pair);
        } else {
            try {
                order.setPair(getPairFromMarketID((String) orderObject.get("marketid")));
            } catch (CryptsyMarketNotFoundException e) {
                LOG.error("Cannot find currency for marketid:" + (String) orderObject.get("marketid"));
                order.setPair(CurrencyList.UNKNOWPAIR);
            }
        }

        order.setInsertedDate(parseDate((String) orderObject.get("created")));

        return order;
    }


    @Override
    public ApiResponse getOrderDetail(String orderID) {
        return getOrdersImpl(null, orderID);
    }

    @Override
    public ApiResponse cancelOrder(String orderID, CurrencyPair pair) {
        ApiResponse apiResponse = new ApiResponse();
        String base = API_BASE_URL;
        String method = API_DELETE_ORDER;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();

        query_args.put("orderid", orderID);

        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            JSONObject httpAnswerJsonSimple = (JSONObject) response.getResponseObject();
            apiResponse.setResponseObject(true);
        } else {
            apiResponse = response;
        }
        return apiResponse;

    }

    @Override
    public ApiResponse getTxFee() {
        return new ApiResponse(true, new Double(0), null);
    }

    @Override
    public ApiResponse getTxFee(CurrencyPair pair) {
        return new ApiResponse(true, new Double(0), null);
    }

    @Override
    public ApiResponse getLastTrades(CurrencyPair pair) {
        return new ApiResponse(true, 0, null);
    }

    @Override
    public ApiResponse getLastTrades(CurrencyPair pair, long startTime) {
        return getTradesImpl(pair, startTime);
    }

    private ApiResponse getTradesImpl(CurrencyPair pair, long startTime) {
        ApiResponse apiResponse = new ApiResponse();
        ArrayList<Trade> tradeList = new ArrayList<Trade>();

        String base = API_BASE_URL;
        String method = API_GET_TRADES;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();

        if (startTime != -1) {
            // yyyy-mm-dd
            query_args.put("startdate", transformTime(startTime));
        }

        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            JSONObject httpAnswerJsonSimple = (JSONObject) response.getResponseObject();
            JSONArray returnJSON = (JSONArray) httpAnswerJsonSimple.get("return");

            for (int i = 0; i < returnJSON.size(); i++) {
                JSONObject tradesObject = (JSONObject) returnJSON.get(i);

                if (tradeBelongsToPair(tradesObject, pair)) {
                    Trade tempTrade = parseTrade(tradesObject, pair);
                    tradeList.add(tempTrade);

                }
            }
            apiResponse.setResponseObject(tradeList);
        } else {
            apiResponse = response;
        }
        return apiResponse;
    }

    private String transformTime(long startTime) {
        //Expecting timestamp long, return a string yyyy-mm-dd
        Date date = new Date(startTime);
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("EST")); //Server timezone
        final String utcTime = sdf.format(date);
        return utcTime;
    }

    @Override
    public ApiResponse isOrderActive(String id) {
        ApiResponse apiResponse = new ApiResponse();
        String base = API_BASE_URL;
        String method = API_GET_ORDER;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();

        query_args.put("orderid", id);

        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            JSONObject httpAnswerJsonSimple = (JSONObject) response.getResponseObject();
            JSONObject returnJSON = (JSONObject) httpAnswerJsonSimple.get("return");
            JSONObject orderInfo = (JSONObject) returnJSON.get("orderinfo");
            boolean active = (boolean) orderInfo.get("active");
            apiResponse.setResponseObject(active);
        } else {
            apiResponse = response;
        }
        return apiResponse;
    }

    @Override
    public ApiResponse getOrderBook(CurrencyPair pair) {
        ApiResponse apiResponse = new ApiResponse();

        String base = API_BASE_URL;
        String method = API_GET_ORDERBOOK;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();
        String marketid = "";
        try {
            marketid = getMarketID(pair);
        } catch (CryptsyMarketNotFoundException e) {
            apiResponse.setError(new ApiError(-1, "Can't find a valid marketid for " + pair.toString() + " :" + e.toString()));
            return apiResponse;
        }

        query_args.put("marketid", marketid);


        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            ArrayList<Order> orderBook = new ArrayList<>();

            JSONObject httpAnswerJsonSimple = (JSONObject) response.getResponseObject();
            JSONObject returnJSON = (JSONObject) httpAnswerJsonSimple.get("return");

            JSONArray asks = (JSONArray) returnJSON.get("sellorders");
            JSONArray bids = (JSONArray) returnJSON.get("buyorders");

            for (Iterator<JSONObject> order = asks.iterator(); order.hasNext(); ) {
                orderBook.add(parseOrderSimple(order.next(), Constant.SELL, pair));
            }
            for (Iterator<JSONObject> order = bids.iterator(); order.hasNext(); ) {
                orderBook.add(parseOrderSimple(order.next(), Constant.BUY, pair));
            }
            apiResponse.setResponseObject(new OrderBook(this.EXCHANGE_NAME, pair, orderBook));

            return apiResponse;

        } else {
            apiResponse = response;
        }
        return apiResponse;
    }

    private Order parseOrderSimple(JSONObject in, String type, CurrencyPair pair) {
        Order out = new Order();

        out.setType(type);
        out.setPair(pair);

        String priceKey = type.equals(Constant.SELL) ? "sellprice" : "buyprice";

        Amount amount = new Amount(Utils.getDouble(in.get("quantity")), pair.getOrderCurrency());
        out.setAmount(amount);

        Amount price = new Amount(Utils.getDouble(in.get(priceKey)), pair.getPaymentCurrency());
        out.setPrice(price);

        /*if (amount.getQuantity() > 10000)
            LOG.warn(type + " " + amount.getQuantity() + " " + pair.getOrderCurrency().getCode() + "@" + Utils.formatNumber(price.getQuantity(), 8) + " " + pair.getPaymentCurrency().getCode());
        */
        out.setCompleted(false);

        return out;
    }

    @Override
    public ApiResponse placeOrders(OrderBatch batch, CurrencyPair pair) {
        return TradeUtils.placeMultipleOrdersSequentiallyImplementation(batch, pair, TradeUtils.INTERVAL_FAST, this);
    }

    @Override
    public ApiResponse placeOrdersParallel(OrderBatch batch, CurrencyPair pair, ArrayList<ApiKeys> keys) {
        return TradeUtils.placeMultipleOrdersParallelImplementation(batch, pair, keys, this);
    }

    @Override
    public ApiResponse clearOrders(CurrencyPair pair) {
        ApiResponse apiResponse = new ApiResponse();
        String base = API_BASE_URL;
        String method = API_CLEAR_ORDERS;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();
        String marketid = "";
        try {
            marketid = getMarketID(pair);
        } catch (CryptsyMarketNotFoundException e) {
            apiResponse.setError(new ApiError(-1, "Can't find a valid marketid for " + pair.toString() + " :" + e.toString()));
            return apiResponse;
        }

        query_args.put("marketid", marketid);


        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            JSONObject httpAnswerJsonSimple = (JSONObject) response.getResponseObject();
            apiResponse.setResponseObject(true);
        } else {
            apiResponse = response;
        }
        return apiResponse;
    }

    @Override
    public ApiError getErrorByCode(int code) {
        return null;
    }

    @Override
    public String getUrlConnectionCheck() {
        return checkConnectionUrl;
    }


    private CurrencyPair getPairFromMarketID(String marketid) throws CryptsyMarketNotFoundException {
        if (marketMap.size() > 0) {
            if (marketMap.containsValue(marketid)) {
                String pairStr = Utils.getKeyByValue(marketMap, marketid);
                pairStr = pairStr.substring(0, 3) + "_" + pairStr.substring(3, 6);
                return CurrencyPair.getCurrencyPairFromString(pairStr);
            } else throw new CryptsyMarketNotFoundException("Can't find marketid:" + marketid);
        } else {
            fetchMarketID(CurrencyList.NBT_BTC);
            return getPairFromMarketID(marketid);
        }
    }

    private Date parseDate(String dateStr) {
        Date toRet = null;
        //Parse the date
        //Sample 2015-11-24 10:16:25

        //Remove the Timezone
        String datePattern = "yyyy-MM-dd HH:mm:ss";
        DateFormat df = new SimpleDateFormat(datePattern, Locale.ENGLISH);
        try {
            toRet = df.parse(dateStr);
        } catch (java.text.ParseException ex) {
            LOG.warn("Cannot parse date" + dateStr + " , setting current date :" + ex.toString());
            toRet = new Date();
        }
        return toRet;
    }

    private Trade parseTrade(JSONObject tradeObj, CurrencyPair pair) {

        Trade trade = new Trade();
        trade.setOrder_id((String) tradeObj.get("order_id"));
        trade.setId((String) tradeObj.get("tradeid"));
        trade.setExchangeName(ExchangeFacade.CRYPTSY);
        trade.setPair(pair);

        trade.setType(((String) tradeObj.get("initiate_ordertype")).toUpperCase());

        trade.setAmount(new Amount(Utils.getDouble(tradeObj.get("quantity")), pair.getPaymentCurrency()));
        trade.setPrice(new Amount(Utils.getDouble(tradeObj.get("tradeprice")), pair.getOrderCurrency()));

        trade.setFee(new Amount(Utils.getDouble(tradeObj.get("fee")), pair.getPaymentCurrency()));

        trade.setDate(parseDate(((String) tradeObj.get("datetime"))));

        return trade;
    }

    private boolean tradeBelongsToPair(JSONObject tradeObj, CurrencyPair pair) {

        String marketid = (String) tradeObj.get("marketid");
        CurrencyPair p = null;
        try {
            p = getPairFromMarketID(marketid);
        } catch (CryptsyMarketNotFoundException e) {
            LOG.error(e.toString());
            return false;
        }

        return pair.equals(p);
    }


    private String getMarketID(CurrencyPair pair) throws CryptsyMarketNotFoundException {
        String pairString = pair.toString();
        if (marketMap.containsKey(pairString)) {
            return marketMap.get(pairString);
        } else {
            try {
                fetchMarketID(pair);
            } catch (CryptsyMarketNotFoundException e) {
                throw e;
            }
            return getMarketID(pair);
        }

    }

    private void fetchMarketID(CurrencyPair pair) throws CryptsyMarketNotFoundException {
        ApiResponse apiResponse = new ApiResponse();
        String base = API_BASE_URL;
        String method = API_GETMARKETS;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();
        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            JSONObject httpAnswerJsonSimple = (JSONObject) response.getResponseObject();
            JSONArray returnArray = (JSONArray) httpAnswerJsonSimple.get("return");
            boolean found = false;
            String marketid = "";
            for (int i = 0; i < returnArray.size(); i++) {
                JSONObject tempObj = (JSONObject) returnArray.get(i);
                Currency orderCurrencyTemp = new Currency(false, (String) tempObj.get("primary_currency_code"), "", "", new ArrayList<String>());
                Currency paymentCurrencyTemp = new Currency(false, (String) tempObj.get("secondary_currency_code"), "", "", new ArrayList<String>());

                CurrencyPair tempCurrencyPair = new CurrencyPair(orderCurrencyTemp, paymentCurrencyTemp);

                marketMap.put(pair.toString(), marketid);

                if (tempCurrencyPair.equals(pair)) {
                    found = true;
                    marketid = (String) tempObj.get("marketid");
                }
            }

            if (!found) {
                throw new CryptsyMarketNotFoundException("Error while retrieving the marketid for " + pair.toString() + ": not found");
            }

        } else {
            throw new CryptsyMarketNotFoundException("Error while retrieving the marketid for " + pair.toString() + ":"
                    + response.getError().toString());
        }

    }


    @Override
    public String query(String base, String method, AbstractMap<String, String> args, boolean needAuth,
                        boolean isGet) {
        String queryResult = TOKEN_BAD_RETURN; //Will return this string in case it fails

        if (this.isFree()) {
            this.setBusy();
            queryResult = service.executeQuery(base, method, args, needAuth, isGet);
            this.setFree();
        } else {
            //Another thread is probably executing a query. Init the retry procedure
            long sleeptime = Settings.RETRY_SLEEP_INCREMENT * 1;
            int counter = 0;
            long startTimeStamp = System.currentTimeMillis();
            LOG.debug(method + " blocked, another call is being processed ");
            boolean exit = false;
            do {
                counter++;
                sleeptime = counter * Settings.RETRY_SLEEP_INCREMENT; //Increase sleep time
                sleeptime += (int) (Math.random() * 200) - 100;// Add +- 100 ms random to facilitate competition
                LOG.debug("Retrying for the " + counter + " time. Sleep for " + sleeptime + "; Method=" + method);
                try {
                    Thread.sleep(sleeptime);
                } catch (InterruptedException e) {
                    LOG.error(e.toString());
                }

                //Try executing the call
                if (this.isFree()) {
                    LOG.debug("Finally the exchange is free, executing query after " + counter + " attempt. Method=" + method);
                    this.setBusy();
                    queryResult = service.executeQuery(base, method, args, needAuth, isGet);
                    this.setFree();
                    break; //Exit loop
                } else {
                    LOG.debug("Exchange still busy : " + counter + " .Will retry soon; Method=" + method);
                    exit = false;
                }
                if (System.currentTimeMillis() - startTimeStamp >= Settings.TIMEOUT_QUERY_RETRY) {
                    exit = true;
                    LOG.error("Method=" + method + " failed too many times and timed out. attempts = " + counter);
                }
            } while (!exit);
        }
        return queryResult;
    }

    private String getTickerUrl(CurrencyPair pair) throws CryptsyMarketNotFoundException {
        return "http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid=" + getMarketID(pair);
    }

    @Override
    public SignedRequest getOpenOrdersRequest(CurrencyPair pair) throws ErrorGeneratingALPRequestException {
        String nonce = Long.toString(System.currentTimeMillis());

        //Create the vocabulary
        HashMap<String, String> query_args = new HashMap<>();
        query_args.put("nonce", nonce);
        query_args.put("apikey", keys.getApiKey());
        query_args.put("method", API_GETORDERS);

        String marketid = "";
        try {
            marketid = getMarketID(pair);
        } catch (CryptsyMarketNotFoundException e) {
            String message = "Can't find a valid marketid for " + pair.toString() + " :" + e.toString();
            LOG.error(message);
            throw new ErrorGeneratingALPRequestException(message);
        }

        query_args.put("marketid", marketid);

        String post_data = TradeUtils.buildQueryString(query_args, ENCODING);

        //Create the signature
        String signature = TradeUtils.signRequest(keys.getPrivateKey(), post_data, SIGN_HASH_FUNCTION, ENCODING);

        return new SignedRequest(query_args, signature);
    }

    @Override
    public void setKeys(ApiKeys keys) {
        this.keys = keys;
    }

    @Override
    public void setApiBaseUrl(String apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
    }

    private class CryptsyService implements ServiceInterface {

        protected ApiKeys keys;

        public CryptsyService(ApiKeys keys) {
            this.keys = keys;
        }

        @Override
        public String executeQuery(String base, String method, AbstractMap<String, String> args, boolean needAuth, boolean isGet) {
            HttpsURLConnection connection = null;
            boolean httpError = false;
            String output;
            int response = 200;
            String answer = null;
            URL queryUrl = null;
            String post_data = "";

            args.put("method", method);
            args.put("nonce", Long.toString(System.currentTimeMillis()));

            try {
                post_data = TradeUtils.buildQueryString(args, ENCODING);
                queryUrl = new URL(base);
            } catch (MalformedURLException mal) {
                LOG.error(mal.toString());
                return null;
            }

            try {
                connection = (HttpsURLConnection) queryUrl.openConnection();
                connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("User-Agent", Settings.APP_NAME);
                connection.setRequestProperty("Accept", "*/*");
                if (needAuth) {
                    connection.setRequestProperty("Key", keys.getApiKey());
                    connection.setRequestProperty("Sign", TradeUtils.signRequest(keys.getPrivateKey(), post_data, SIGN_HASH_FUNCTION, ENCODING));
                }
                connection.setRequestProperty("Connection", "close");
                connection.setRequestProperty("Host", "cryptsy.com");

                connection.setDoOutput(true);
                connection.setDoInput(true);

                if (isGet) {
                    connection.setRequestMethod("GET");
                } else {
                    connection.setRequestMethod("POST");
                    DataOutputStream os = new DataOutputStream(connection.getOutputStream());
                    os.writeBytes(post_data);
                    os.flush();
                    os.close();
                }
            } catch (ProtocolException pe) {
                LOG.error(pe.toString());
                return answer;
            } catch (IOException io) {
                LOG.error((io.toString()));
                return answer;
            }

            BufferedReader br = null;
            try {
                if (connection.getResponseCode() >= 400) {
                    httpError = true;
                    response = connection.getResponseCode();
                    answer = "";
                    br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                } else {
                    answer = "";
                    br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                }
            } catch (IOException io) {
                LOG.error(io.toString());
                return answer;
            }

            if (httpError) {
                LOG.error("Query to : " + queryUrl
                        + "\nHTTP Response : " + Objects.toString(response));
            }

            try {
                while ((output = br.readLine()) != null) {
                    answer += output;
                }
            } catch (IOException io) {
                LOG.error(io.toString());
                return null;
            }

            connection.disconnect();
            connection = null;

            return answer;
        }
    }
}
