package com.nubits.nubot.pricefeeds;

import com.nubits.nubot.models.LastPrice;

import java.util.ArrayList;

/**
 * A batch of prices from feeds
 */
public class PriceBatch {

    public ArrayList<LastPrice> priceList;
    public int mainFeedIndex = 0;
    private boolean mainFeedValid;

    public PriceBatch() {

    }

    public void setPrices(ArrayList<LastPrice> priceList) {
        this.priceList = priceList;
    }

    public void setMainFeedValid(boolean valid) {
        this.mainFeedValid = valid;
    }

    public PriceBatch(ArrayList<LastPrice> currentPriceList) {
        this.priceList = currentPriceList;
        //this.mainFeedIndex = 0;
    }

    public LastPrice getMain() {
        return this.priceList.get(this.mainFeedIndex);
    }

    public LastPrice get(int i) {
        return this.priceList.get(i);
    }

    public int size() {
        return this.priceList.size();
    }

    public LastPrice getMainPrice() {
        return priceList.get(mainFeedIndex);
    }

    /**
     * get the list of prices, without the main
     *
     * @return
     */
    public ArrayList<LastPrice> withoutMain() {

        return without(mainFeedIndex);
    }

    public ArrayList<LastPrice> without(int index) {

        ArrayList<LastPrice> others = new ArrayList<>();
        for (int i = 0; i < priceList.size(); i++) {
            if (i != index) {
                LastPrice tempPrice = priceList.get(i);
                others.add(tempPrice);
            }
        }
        return others;
    }
}
