/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.pricefeeds.feedservices;

import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.Amount;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.pricefeeds.FeedCallException;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.utils.Utils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *
 */
public class PoloniexPriceFeed extends AbstractPriceFeed {
    private static final Logger LOG = LoggerFactory.getLogger(PoloniexPriceFeed.class.getName());

    public PoloniexPriceFeed() {
        refreshMinTime = Settings.FEEDFETCH_DEFAULT_INTERVAL;
        name = FeedFacade.PoloniexFeed;
    }

    @Override
    public LastPrice forceFetchLastPrice(CurrencyPair pair) throws FeedCallException {
        if (pair.equals(CurrencyList.ETH_USD) ||
                pair.equals(CurrencyList.XRP_USD)
                || pair.equals(CurrencyList.LTC_USD)) {
            //IF ETH,LTC or XRP, then first get XBTUSD String htmlString;

            try {

                try {
                    String allTickers = Utils.getHTML("https://poloniex.com/public?command=returnTicker", true);

                    //Define a feed for getting lastprice
                    try {
                        String mainFeedBTCName = CurrencyList.BTC.getDefaultMainFeedName();
                        AbstractPriceFeed mainFeedBTC = FeedFacade.getFeed(mainFeedBTCName);
                        double btcusdRate = mainFeedBTC.forceFetchLastPrice(CurrencyList.BTC_USD).getPrice().getQuantity();
                        double cryptoToBtcRate = getPriceImpl(allTickers, "BTC_" + pair.getOrderCurrency().getCode());

                        double last = Utils.round(btcusdRate * cryptoToBtcRate);

                        LastPrice tmp = new LastPrice(false, name, pair.getOrderCurrency(), new Amount(last, CurrencyList.USD));
                        return tmp;
                    } catch (NuBotConfigException e) {
                        throw new FeedCallException(e.toString());
                    }


                } catch (IOException ex) {
                    LOG.error(ex.toString());
                    throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price."));
                }


            } catch (FeedCallException e) {
                throw e;
            }

        } else {
            throw new FeedCallException("Pair " + pair.toString() + " not supported by " + name);
        }


    }

    private double getPriceImpl(String allTickers, String seek) throws FeedCallException {

        seek = seek.toUpperCase();
        JSONParser parser = new JSONParser();
        try {
            JSONObject httpAnswerJson = (JSONObject) (parser.parse(allTickers));
            if (httpAnswerJson.containsKey(seek)) {
                JSONObject ticketObject = (JSONObject) httpAnswerJson.get(seek);
                return Utils.getDouble(ticketObject.get("last"));
            } else {
                throw new FeedCallException("Cannot find pair:" + seek + " in the public ticker");
            }

        } catch (Exception ex) {
            LOG.error(allTickers);
            LOG.error(ex.toString());
            throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price. (parseError)"));
        }
    }
}
