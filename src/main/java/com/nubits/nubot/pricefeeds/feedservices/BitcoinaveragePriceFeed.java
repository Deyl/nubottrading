/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.pricefeeds.feedservices;

import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.Amount;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.pricefeeds.FeedCallException;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.utils.Utils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


public class BitcoinaveragePriceFeed extends AbstractPriceFeed {

    private static final Logger LOG = LoggerFactory.getLogger(BitcoinaveragePriceFeed.class.getName());

    public BitcoinaveragePriceFeed() {
        name = FeedFacade.BitcoinaveragePriceFeed;
        refreshMinTime = Settings.FEEDFETCH_DEFAULT_INTERVAL;
        lastRequest = 0L;
    }

    @Override
    public LastPrice forceFetchLastPrice(CurrencyPair pair) throws FeedCallException {
        String htmlString;
        try {
            htmlString = Utils.getHTML("https://api.bitcoinaverage.com/ticker/global/USD", true);
        } catch (IOException ex) {
            LOG.error(ex.toString());
            throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price."));
        }
        JSONParser parser = new JSONParser();
        try {
            JSONObject httpAnswerJson = (JSONObject) (parser.parse(htmlString));
            double last = Utils.getDouble(httpAnswerJson.get("last"));
            LastPrice tmp = new LastPrice(false, name, CurrencyList.BTC, new Amount(last, CurrencyList.USD));
            return tmp;
        } catch (Exception ex) {
            LOG.error(ex.toString());
            throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price (parse error)."));
        }
    }

}
