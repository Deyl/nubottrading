/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.pricefeeds.feedservices;


import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.*;
import com.nubits.nubot.pricefeeds.FeedCallException;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.trading.Ticker;
import com.nubits.nubot.trading.keys.ApiKeys;
import com.nubits.nubot.trading.wrappers.BterWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BterPriceFeed extends AbstractPriceFeed {

    private static final Logger LOG = LoggerFactory.getLogger(BterPriceFeed.class.getName());

    public BterWrapper trader;

    public BterPriceFeed() {
        name = FeedFacade.BterPriceFeed;
        refreshMinTime = Settings.FEEDFETCH_DEFAULT_INTERVAL;
        ApiKeys keys = new ApiKeys("a", "b");

        trader = (BterWrapper) new BterWrapper(keys);
    }

    @Override
    public LastPrice forceFetchLastPrice(CurrencyPair pair) throws FeedCallException {
        try {
            ApiResponse lastPriceResponse = trader.getLastPrice(pair);
            if (lastPriceResponse.isPositive()) {
                Ticker ticker = (Ticker) lastPriceResponse.getResponseObject();
                double last = ticker.getLast();
                LastPrice tmp = new LastPrice(false, name, pair.getOrderCurrency(), new Amount(last, CurrencyList.USD));
                return tmp;
            } else {
                LOG.error(lastPriceResponse.getError().toString());
                throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price."));
            }

        } catch (Exception ex) {
            LOG.error(ex.toString());
            throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price. (parseError)"));
        }
    }

    private String getUrl(CurrencyPair pair) {
        return "http://data.bter.com/api/1/ticker/" + pair.toStringSep();
    }
}
