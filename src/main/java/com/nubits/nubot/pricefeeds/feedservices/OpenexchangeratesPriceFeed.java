/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.pricefeeds.feedservices;


import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.CredentialManager;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.Amount;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.pricefeeds.FeedCallException;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.utils.Utils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class OpenexchangeratesPriceFeed extends AbstractPriceFeed {

    private static final Logger LOG = LoggerFactory.getLogger(OpenexchangeratesPriceFeed.class.getName());

    public OpenexchangeratesPriceFeed() {
        refreshMinTime = Settings.FEEDFETCH_FIAT_INTERVAL;
        name = FeedFacade.OpenexchangeratesPriceFeed;
    }

    @Override
    public LastPrice forceFetchLastPrice(CurrencyPair pair) throws FeedCallException {
        String url = getUrl(pair);
        String htmlString;
        try {
            LOG.trace("feed fetching from URL: " + url);
            htmlString = Utils.getHTML(url, true);
        } catch (IOException ex) {
            LOG.error(ex.toString());
            throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price."));
        }
        JSONParser parser = new JSONParser();
        boolean found = false;
        try {
            JSONObject httpAnswerJson = (JSONObject) (parser.parse(htmlString));

            String lookingfor = pair.getOrderCurrency().getCode().toUpperCase();
            JSONObject rates = (JSONObject) httpAnswerJson.get("rates");
            if (rates.containsKey(lookingfor)) {
                double last = (Double) rates.get(lookingfor);
                LOG.trace("last " + last);
                last = Utils.round(1 / last);
                LastPrice tmp = new LastPrice(false, name, pair.getOrderCurrency(), new Amount(last, CurrencyList.USD));
                return tmp;
            } else {
                LOG.warn("Cannot find currency :" + lookingfor + " on feed :" + name);
                throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price. (parseError)"));
            }


        } catch (ParseException ex) {
            LOG.error(ex.toString());
            return new LastPrice(true, name, pair.getOrderCurrency(), null);
        }
    }

    private String getUrl(CurrencyPair pair) {
        String key = Global.credentialManager.get(CredentialManager.OPEN_EXCHANGE_RATES_APP_ID_KEY);
        return "https://openexchangerates.org/api/latest.json?app_id=" + key;
    }
}
