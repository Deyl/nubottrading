/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.pricefeeds.feedservices;


import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.CredentialManager;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.Amount;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.pricefeeds.FeedCallException;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.utils.Utils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ExchangeratelabPriceFeed extends AbstractPriceFeed {

    private static final Logger LOG = LoggerFactory.getLogger(ExchangeratelabPriceFeed.class.getName());

    public ExchangeratelabPriceFeed() {
        name = FeedFacade.ExchangeratelabPriceFeed;
        refreshMinTime = Settings.FEEDFETCH_FIAT_INTERVAL;
    }

    @Override
    public LastPrice forceFetchLastPrice(CurrencyPair pair) throws FeedCallException {
        String url = getUrl(pair);
        String htmlString;
        try {
            htmlString = Utils.getHTML(url, true);
        } catch (IOException ex) {
            LOG.error(ex.toString());
            throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price. "));
        }
        JSONParser parser = new JSONParser();
        try {
            JSONObject httpAnswerJson = (JSONObject) (parser.parse(htmlString));
            JSONArray array = (JSONArray) httpAnswerJson.get("rates");

            String lookingfor = pair.getOrderCurrency().getCode().toUpperCase();

            boolean found = false;
            double rate = -1;
            for (int i = 0; i < array.size(); i++) {
                JSONObject temp = (JSONObject) array.get(i);
                String tempCurrency = (String) temp.get("to");
                if (tempCurrency.equalsIgnoreCase(lookingfor)) {
                    found = true;
                    rate = Utils.getDouble((Double) temp.get("rate"));
                    rate = Utils.round(1 / rate);
                }
            }


            if (found) {
                LastPrice tmp = new LastPrice(false, name, pair.getOrderCurrency(), new Amount(rate, CurrencyList.USD));
                return tmp;
            } else {
                LOG.warn("Cannot find currency " + lookingfor + " on feed " + name);
                throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price."));
            }
        } catch (Exception ex) {
            LOG.error(ex.toString());
            throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price. (parseError)"));
        }
    }

    private String getUrl(CurrencyPair pair) {
        return "http://api.exchangeratelab.com/api/current?apikey=" + Global.credentialManager.get(CredentialManager.EXCHANGE_RATE_LAB_KEY);
    }
}
