package com.nubits.nubot.pricefeeds;

import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Utilities for feed quality
 */
public class FeedQuality {

    private static final Logger LOG = LoggerFactory.getLogger(FeedQuality.class.getName());

    public static LastPrice handleNormal(double distanceTreshold, PriceBatch priceBatch) {
        //All feeds returned a positive value
        //Check if mainPrice is close enough to the others
        // I am assuming that mainPrice is the first element of the list
        if (sanityCheck(distanceTreshold, priceBatch, priceBatch.mainFeedIndex)) {
            //mainPrice is reliable compared to the others
            LastPrice goodPrice = priceBatch.getMainPrice();
            return goodPrice;

        } else {
            //mainPrice is not reliable compared to the others
            //Check if other backup prices are close enough to each other
            return check(distanceTreshold, priceBatch);
        }
    }

    public static LastPrice check(double distanceThreshold, PriceBatch priceBatch) {
        LastPrice goodPrice = null;
        //0 left out. assumes to be main
        for (int l = 1; l < priceBatch.priceList.size(); l++) {
            if (sanityCheck(distanceThreshold, priceBatch, l)) {
                goodPrice = priceBatch.priceList.get(l);
                break;
            }
        }

        return goodPrice;
    }

    public static LastPrice handleFail(double distanceTreshold, PriceBatch priceBatch) {
        LOG.debug("handle fail " + distanceTreshold);
        //One or more feed returned an error value
        if (priceBatch.priceList.size() < 2) { // if only one or 0 feeds are positive
            return null;
        }

        if (priceBatch.priceList.size() == 2) { // if only 2 values are available
            double p1 = priceBatch.priceList.get(0).getPrice().getQuantity();
            double p2 = priceBatch.priceList.get(1).getPrice().getQuantity();
            LOG.debug("p1: " + p1 + " p2 " + p2);
            if (closeEnough(distanceTreshold, p1, p2)) {
                LastPrice goodPrice = priceBatch.getMainPrice();
                return goodPrice;
            } else {
                String message = "Only two valid feeds and didn't pass sanity check .";
                LOG.error(message);
            }
        } else if (priceBatch.priceList.size() > 2) { // more than two
            return check(distanceTreshold, priceBatch);
        }

        String message = "Cannot handle failure successfully";
        LOG.error(message);
        return null;
    }

    public static boolean moreThanHalf(int countOk, int num) {
        boolean overallOk = false; //is considered ok if the mainPrice is closeEnough to more than a half of backupPrices
        //Need to distinguish even and odd
        if (num % 2 == 0) {
            if (countOk >= num / 2) {
                overallOk = true;
            }
        } else {
            if (countOk > num / 2) {
                overallOk = true;
            }
        }
        return overallOk;
    }

    /**
     * Measure if mainPrice is close to other two values
     */
    public static boolean sanityCheck(double distanceThreshold, PriceBatch priceBatch, int index) {

        int num = priceBatch.priceList.size() - 1;
        double mainPrice = priceBatch.getMainPrice().getPrice().getQuantity();
        ArrayList<LastPrice> backups = priceBatch.without(index);

        //Test mainPrice vs backup sources

        int countOk = 0;
        for (LastPrice tempPrice : backups) {
            double temp = tempPrice.getPrice().getQuantity();
            if (closeEnough(distanceThreshold, mainPrice, temp))
                countOk++;
        }

        return moreThanHalf(countOk, num);
    }

    public static boolean closeEnough(double distanceThreshold, double mainPrice, double temp) {
        //if temp differs from mainPrice for more than a threshold%, return false
        double distance = Utils.percentagePriceVariation(mainPrice, temp);

        if (distance > distanceThreshold) {
            return false;
        } else {
            return true;
        }
    }
}
