/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.testsmanual;


import com.nubits.nubot.bot.Global;
import com.nubits.nubot.exchanges.ExchangeFacade;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.models.*;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.trading.Ticker;
import com.nubits.nubot.trading.TradeInterface;
import com.nubits.nubot.trading.keys.ApiKeys;
import com.nubits.nubot.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class WrapperTestUtils {

    private static final Logger LOG = LoggerFactory.getLogger(WrapperTestUtils.class.getName());

    public static void testGetAvailableBalances(CurrencyPair pair) {
        //Get all the balances  associated with the account
        ApiResponse balancesResponse = Global.trade.getAvailableBalances(pair);
        if (balancesResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.getBalance() ");
            PairBalance balance = (PairBalance) balancesResponse.getResponseObject();

            LOG.info(balance.toString());

        } else {
            LOG.error(balancesResponse.getError().toString());
        }
    }

    public static void testGetAvailableBalance(Currency cur) {
        //Get the USD balance associated with the account
        ApiResponse balanceResponse = Global.trade.getAvailableBalance(cur);
        if (balanceResponse.isPositive()) {
            LOG.info("Positive response from TradeInterface.getBalance(Currency cur) ");
            Amount balance = (Amount) balanceResponse.getResponseObject();

            LOG.info(balance.toString());
        } else {
            LOG.error(balanceResponse.getError().toString());
        }
    }

    public static void testGetLastPrice(CurrencyPair pair) {
        //Get lastPrice for a given CurrencyPair
        ApiResponse lastPriceResponse = Global.trade.getLastPrice(pair);
        if (lastPriceResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.getLastPrice(CurrencyPair pair) ");
            Ticker ticker = (Ticker) lastPriceResponse.getResponseObject();
            LOG.info("Last price : 1 " + pair.getOrderCurrency().getCode() + " = "
                    + ticker.getLast() + " " + pair.getPaymentCurrency().getCode());
            LOG.info("ask  : 1 " + pair.getOrderCurrency().getCode() + " = "
                    + ticker.getAsk() + " " + pair.getPaymentCurrency().getCode());
            LOG.info("bid  : 1 " + pair.getOrderCurrency().getCode() + " = "
                    + ticker.getBid() + " " + pair.getPaymentCurrency().getCode());

        } else {
            LOG.error(lastPriceResponse.getError().toString());
        }

    }

    public static void testSell(double amountSell, double priceSell, CurrencyPair pair) {
        //Place a sell order


        ApiResponse sellResponse = Global.trade.sell(pair, amountSell, priceSell);
        if (sellResponse.isPositive()) {

            LOG.info("\nPositive response  from TradeInterface.sell(...) ");
            LOG.info("Submit order : "
                    + "sell" + amountSell + " " + pair.getOrderCurrency().getCode()
                    + " @ " + priceSell + " " + pair.getPaymentCurrency().getCode());

            String sellResponseString = (String) sellResponse.getResponseObject();
            LOG.info("Response = " + sellResponseString);
        } else {
            LOG.error(sellResponse.getError().toString());
        }
    }

    public static void testBuy(double amountBuy, double priceBuy, CurrencyPair pair) {
        //Place a buy order

        ApiResponse buyResponse = Global.trade.buy(pair, amountBuy, priceBuy);
        if (buyResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.buy(...) ");
            LOG.info("Submit order : "
                    + "buy" + amountBuy + " " + pair.getOrderCurrency().getCode()
                    + " @ " + priceBuy + " " + pair.getPaymentCurrency().getCode());
            String buyResponseString = (String) buyResponse.getResponseObject();
            LOG.info("Response = " + buyResponseString);

        } else {
            LOG.error(buyResponse.getError().toString());
        }
    }

    public static void testGetActiveOrders() {
        //Get active orders
        ApiResponse activeOrdersResponse = Global.trade.getActiveOrders();
        if (activeOrdersResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.getActiveOrders() ");
            ArrayList<Order> orderList = (ArrayList<Order>) activeOrdersResponse.getResponseObject();

            LOG.info("Active orders : " + orderList.size());
            for (int i = 0; i < orderList.size(); i++) {
                Order tempOrder = orderList.get(i);
                LOG.info(tempOrder.toString());
            }

        } else {
            LOG.error(activeOrdersResponse.getError().toString());
        }
    }

    public static void testGetActiveOrders(CurrencyPair pair, boolean printThem) {
        //Get active orders associated with a specific CurrencyPair
        ApiResponse activeOrdersUSDNTBResponse = Global.trade.getActiveOrders(pair);
        if (activeOrdersUSDNTBResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.getActiveOrders(CurrencyPair pair) ");
            ArrayList<Order> orderListUSDNBT = (ArrayList<Order>) activeOrdersUSDNTBResponse.getResponseObject();

            LOG.info("Active orders : " + orderListUSDNBT.size());
            if (printThem) {
                for (int i = 0; i < orderListUSDNBT.size(); i++) {
                    Order tempOrder = orderListUSDNBT.get(i);
                    LOG.info(tempOrder.toString());
                }
            }
        } else {
            LOG.error(activeOrdersUSDNTBResponse.getError().toString());
        }
    }

    public static void testGetOrderDetail(String order_id_detail) {
        //Get the order details for a specific order_id
        ApiResponse orderDetailResponse = Global.trade.getOrderDetail(order_id_detail);
        if (orderDetailResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.getOrderDetail(id) ");
            Order order = (Order) orderDetailResponse.getResponseObject();
            LOG.info(order.toString());
        } else {
            LOG.error(orderDetailResponse.getError().toString());
        }
    }

    public static void testCancelOrder(String order_id_delete, CurrencyPair pair) {
        //Cancel an order
        ApiResponse deleteOrderResponse = Global.trade.cancelOrder(order_id_delete, pair);
        if (deleteOrderResponse.isPositive()) {
            boolean deleted = (boolean) deleteOrderResponse.getResponseObject();

            if (deleted) {
                LOG.info("Order deleted succesfully");
            } else {
                LOG.info("Could not delete order");
            }

        } else {
            LOG.error(deleteOrderResponse.getError().toString());
        }
    }

    public static void testGetTxFee() {
        //Get current trascation fee
        ApiResponse txFeeResponse = Global.trade.getTxFee();
        if (txFeeResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.getTxFee()");
            double txFee = (Double) txFeeResponse.getResponseObject();
            LOG.info("Trasaction fee = " + txFee + "%");
        } else {
            LOG.error(txFeeResponse.getError().toString());
        }
    }

    public static void testGetTxFeeWithArgs(CurrencyPair pair) {
        //Get the current transaction fee associated with a specific CurrencyPair
        ApiResponse txFeeNTBUSDResponse = Global.trade.getTxFee(pair);
        if (txFeeNTBUSDResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.getTxFee(CurrencyPair pair)");
            double txFeeUSDNTB = (Double) txFeeNTBUSDResponse.getResponseObject();
            LOG.info("Trasaction fee = " + txFeeUSDNTB + "%");
        } else {
            LOG.error(txFeeNTBUSDResponse.getError().toString());
        }
    }

    public static void testIsOrderActive(String orderId) {
        //Check if orderId is active
        ApiResponse orderDetailResponse = Global.trade.isOrderActive(orderId);
        if (orderDetailResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.isOrderActive(id) ");
            boolean exist = (boolean) orderDetailResponse.getResponseObject();
            LOG.info("Order " + orderId + "  active? " + exist);
        } else {
            LOG.error(orderDetailResponse.getError().toString());
        }
    }

    public static void testClearAllOrders(CurrencyPair pair) {
        LOG.debug("TestClearAllOrders");
        ApiResponse deleteOrdersResponse = Global.trade.clearOrders(pair);
        if (deleteOrdersResponse.isPositive()) {
            boolean deleted = (boolean) deleteOrdersResponse.getResponseObject();

            if (deleted) {
                LOG.info("Order clear request successfully");
            } else {
                LOG.error("Could not submit request to clear orders");
            }

        } else {
            LOG.error(deleteOrdersResponse.getError().toString());
        }
    }

    public static void testGetLastTrades(CurrencyPair pair) {
        //Get active orders
        ApiResponse activeOrdersResponse = Global.trade.getLastTrades(pair);
        if (activeOrdersResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.getLastTrades(pair) ");
            ArrayList<Trade> tradeList = (ArrayList<Trade>) activeOrdersResponse.getResponseObject();
            LOG.info("Last 24h trades : " + tradeList.size());
            for (int i = 0; i < tradeList.size(); i++) {
                Trade tempTrade = tradeList.get(i);
                LOG.info(tempTrade.toString());
            }
        } else {
            LOG.error(activeOrdersResponse.getError().toString());
        }
    }

    public static void testGetLastTrades(CurrencyPair pair, long startTime) {
        //Get active orders
        ApiResponse activeOrdersResponse = Global.trade.getLastTrades(pair, startTime);
        if (activeOrdersResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.getLastTrades(pair,startTime) ");
            ArrayList<Trade> tradeList = (ArrayList<Trade>) activeOrdersResponse.getResponseObject();
            LOG.info("Last trades from " + startTime + " : " + tradeList.size());
            for (int i = 0; i < tradeList.size(); i++) {
                Trade tempTrade = tradeList.get(i);
                LOG.info(tempTrade.toString());
            }
        } else {
            LOG.error(activeOrdersResponse.getError().toString());
        }
    }

    public static void testPlaceOrders(OrderBatch batch, CurrencyPair pair) {
        LOG.info("Trying to place " + batch.getOrders().size() + " orders...");

        ApiResponse placeOrdersResponse = Global.trade.placeOrders(batch, pair);
        if (placeOrdersResponse.isPositive()) {
            LOG.info("\nPositive response  from TradeInterface.placeOrders(orders,pair) ");
            MultipleOrdersResponse multipleOrdersResponse = (MultipleOrdersResponse) placeOrdersResponse.getResponseObject();
            //Test high level methods:
            LOG.info("Time elapsed : " + Utils.roundPlaces(multipleOrdersResponse.getTimeElapsed() / 1000, 2) + " s");
            LOG.info("Orders requested: " + multipleOrdersResponse.countOrdersRequested());
            LOG.info("Are all orders placed Correctly?  " + multipleOrdersResponse.areAllOrdersPlacedCorrectly());
            LOG.info(multipleOrdersResponse.countOrdersPlacedSuccessfully() + " out of " + multipleOrdersResponse.countOrdersRequested() + "orders correctly placed. ");

            LOG.info("Orders correctly placed list : -------- \n");
            multipleOrdersResponse.printOrdersPlacedSuccessfully();

            LOG.info(multipleOrdersResponse.countOrdersPlacedWithErrors() + " out of " + multipleOrdersResponse.countOrdersRequested() + "orders failed. ");
            LOG.info("Orders placed with errors list : -------- \n");
            multipleOrdersResponse.printOrdersPlacedWithErrors();


            //How to acces objects
            ArrayList<OrderPlaced> ordersRequested = multipleOrdersResponse.getAllOrdersRequested();
            ArrayList<OrderPlaced> ordersFailed = multipleOrdersResponse.getOrdersPlacedWithErrors();
            ArrayList<OrderPlaced> ordersSuccessful = multipleOrdersResponse.getOrdersPlacedSuccessfully();


            //Example : Access first order requested
            if (ordersRequested.size() != 0) {
                LOG.info("First order requested=" + ordersRequested.get(0).getOrder().toString());
            }

            //Example : Access the order id of first order placed
            if (ordersSuccessful.size() != 0) {
                LOG.info("First order placed =" + ordersSuccessful.get(0).getOrder().toString());
                LOG.info("First order placed ID =" + ordersSuccessful.get(0).getResponse().getResponseObject().toString());
            }

            //Example : Access the error in response of first order error
            if (ordersFailed.size() != 0) {
                LOG.info("First order failed =" + ordersFailed.get(0).getOrder().toString());
                LOG.error("First order placed error =" + ordersFailed.get(0).getResponse().getError().toString());
            }


            LOG.info("First order response=" + ordersRequested.get(0).getResponse().toString());


        } else {
            LOG.error(placeOrdersResponse.getError().toString());
        }

    }

    public static void testPlaceAndClearAllOrders(CurrencyPair pair) {

        //clear old orders if any
        testClearAllOrders(pair);


        // place a few orders
        for (int i = 0; i <= 5; i++) {
            testSell(0.1, 0.004, pair);
            try {
                Thread.sleep(400);
            } catch (InterruptedException ex) {
                LOG.error(ex.toString());
            }
        }

        for (int i = 0; i <= 5; i++) {
            testBuy(0.1, 0.001, pair);
            try {
                Thread.sleep(400);
            } catch (InterruptedException ex) {
                LOG.error(ex.toString());
            }
        }


        //Wait 4 secs
        try {
            Thread.sleep(4000);
        } catch (InterruptedException ex) {
            LOG.error(ex.toString());
        }

        //try to clear orders
        testClearAllOrders(pair);
    }

    public static void testGetOrderBook(CurrencyPair pair, boolean draw) {
        ApiResponse orderBookResponse = Global.trade.getOrderBook(pair);
        if (orderBookResponse.isPositive()) {
            LOG.info("Positive response from " + Global.options.getExchangeName());
            OrderBook orderBook = (OrderBook) orderBookResponse.getResponseObject();
            LOG.info("Order book toString:\n" + orderBook.toString());
            LOG.info("Total Ask Liquidity = " + orderBook.getTotalLiquidity(Constant.SELL) + " " + pair.getOrderCurrency().getCode());
            LOG.info("Total Bid Liquidity = " + orderBook.getTotalLiquidity(Constant.BUY) + " " + pair.getOrderCurrency().getCode());
            LOG.info("Top Ask Order = " + orderBook.getTopAsk());
            LOG.info("Top Bid Order = " + orderBook.getTopBid());
            LOG.info("Ask Orders sorted by Price Desc = " + orderBook.getAsks(Constant.DESC));
            LOG.info("Bid Orders sorted by Price Desc = " + orderBook.getBids(Constant.DESC));

            if (draw) {
                orderBook.draw();
            }
        } else {
            LOG.error(orderBookResponse.getError().toString());
        }
    }

    public static void configureExchange(String exchangeName) throws NuBotConfigException {


        //Create the ApiKeys object reading from option files
        ApiKeys keys = new ApiKeys(Global.options.getApiSecret(), Global.options.getApiKey());

        //Create a new TradeInterface object using the custom implementation
        TradeInterface ti = ExchangeFacade.getInterfaceByName(exchangeName, keys);

        //Assign the keys to the TradeInterface
        ti.setKeys(keys);

        //Assign the TradeInterface to the exchange
        Global.trade = ti;

    }
}
