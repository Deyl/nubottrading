/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.testsmanual;

import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.utils.InitTests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class TestDefaultFeeds {
    //define Logging by using predefined Settings which points to an XML
    static {
        System.setProperty("logback.configurationFile", Settings.TEST_LOGXML);
    }

    private static final Logger LOG = LoggerFactory.getLogger(TestDefaultFeeds.class.getName());

    public static void main(String[] args) {
        InitTests.setLoggingFilename(TestDefaultFeeds.class.getSimpleName());
        LOG.warn("\n\nGet default mainFeed and backupFeeds based on CurrencyPair");
        for (int i = 0; i < CurrencyList.allPairs.size(); i++) {
            CurrencyPair tempPair = CurrencyList.allPairs.get(i);
            try {
                LOG.info(tempPair.toString() + " mainFeed=" + FeedFacade.getDefaultMainFeed(tempPair));
                LOG.info(tempPair.toString() + " backupFeeds=" + FeedFacade.getDefaultBackupFeeds(tempPair));
                LOG.info("----------------------------------------------------------------------------------");

            } catch (Exception e) {
                LOG.error("Failed to retrieve feeds for the pair " + tempPair.toString() + " : " + e.getMessage());
            }
        }

    }
}
