/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.testsmanual;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.CredentialManager;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.utils.InitTests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class TestCredentialsManager {
    //define Logging by using predefined Settings which points to an XML
    static {
        System.setProperty("logback.configurationFile", Settings.TEST_LOGXML);
    }


    private static final Logger LOG = LoggerFactory.getLogger(TestCredentialsManager.class.getName());

    public static void main(String[] args) {
        InitTests.setLoggingFilename(TestCredentialsManager.class.getSimpleName());

        try {
            Global.credentialManager = CredentialManager.createCredentialManager(false);
        } catch (Exception e) {
            LOG.error(e.toString());
        }

        LOG.warn("Test authorized Credential Manager");
        LOG.info(Global.credentialManager.get(CredentialManager.KEYSTORE_ENCRYPTION_PASS_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.SMTP_USERNAME_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.SMTP_PASSWORD_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.SMTP_HOST_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.HIPCHAT_NOTIFICATIONS_ROOM_ID_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.HIPCHAT_NOTIFICATIONS_ROOM_TOKEN_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.HIPCHAT_CRITICAL_ROOM_ID_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.HIPCHAT_CRITICAL_ROOM_TOKEN_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.OPEN_EXCHANGE_RATES_APP_ID_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.EXCHANGE_RATE_LAB_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.STREAMER_TOKEN_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.GITTER_ACCESS_TOKEN));
        LOG.info(Global.credentialManager.get(CredentialManager.GITTER_CRITICAL_ROOM_ID));
        LOG.info(Global.credentialManager.get(CredentialManager.GITTER_NOTIFICATIONS_ROOM_ID));
        LOG.info(Global.credentialManager.get(CredentialManager.LIQUIDITY_PROVIDER_KEY));

        try {
            Global.credentialManager = CredentialManager.createCredentialManager(true);
        } catch (Exception e) {
            LOG.error(e.toString());
        }

        LOG.warn("Test authorized User-Overridden Credential  Manager");

        LOG.info(Global.credentialManager.get(CredentialManager.KEYSTORE_ENCRYPTION_PASS_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.SMTP_USERNAME_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.SMTP_PASSWORD_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.SMTP_HOST_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.HIPCHAT_NOTIFICATIONS_ROOM_ID_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.HIPCHAT_NOTIFICATIONS_ROOM_TOKEN_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.HIPCHAT_CRITICAL_ROOM_ID_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.HIPCHAT_CRITICAL_ROOM_TOKEN_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.OPEN_EXCHANGE_RATES_APP_ID_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.EXCHANGE_RATE_LAB_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.STREAMER_TOKEN_KEY));
        LOG.info(Global.credentialManager.get(CredentialManager.GITTER_ACCESS_TOKEN));
        LOG.info(Global.credentialManager.get(CredentialManager.GITTER_CRITICAL_ROOM_ID));
        LOG.info(Global.credentialManager.get(CredentialManager.GITTER_NOTIFICATIONS_ROOM_ID));
        LOG.info(Global.credentialManager.get(CredentialManager.LIQUIDITY_PROVIDER_KEY));

    }
}
