/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used in placeOrders(), it contains the OrderToPlace object and its response
 */
public class OrderPlaced {
    private static final Logger LOG = LoggerFactory.getLogger(OrderPlaced.class.getName());

    private OrderToPlace order;
    private ApiResponse response;

    public OrderPlaced(OrderToPlace order, ApiResponse response) {
        this.order = order;
        this.response = response;
    }

    public OrderToPlace getOrder() {
        return order;
    }

    public void setOrder(OrderToPlace order) {
        this.order = order;
    }

    public ApiResponse getResponse() {
        return response;
    }

    public void setResponse(ApiResponse response) {
        this.response = response;
    }


}
