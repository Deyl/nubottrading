/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * A list of orders with a pointer to Tier1 orders
 */
public class OrderBatch {
    private static final Logger LOG = LoggerFactory.getLogger(OrderBatch.class.getName());

    private ArrayList<OrderToPlace> orders;
    private int wallIndex; // -1 indicates noWall in this batch

    public OrderBatch(ArrayList<OrderToPlace> orders, int wallIndex) {
        this.orders = orders;
        this.wallIndex = wallIndex;
    }

    public ArrayList<OrderToPlace> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<OrderToPlace> orders) {
        this.orders = orders;
    }

    public int getWallIndex() {
        return wallIndex;
    }

    public void setWallIndex(int wallIndex) {
        this.wallIndex = wallIndex;
    }

    public OrderBatch swap() {
        OrderBatch out = null;

        ArrayList<OrderToPlace> ordersSwapped = new ArrayList<>();
        int wallIndex = this.getWallIndex();

        for (int i = 0; i < this.getOrders().size(); i++) {
            OrderToPlace tempOrderToPlace = this.getOrders().get(i);
            ordersSwapped.add(tempOrderToPlace.swap());
        }

        out = new OrderBatch(ordersSwapped, wallIndex);

        return out;
    }

}
