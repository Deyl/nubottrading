/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.options;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.exchanges.ExchangeFacade;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.notifications.MailNotifications;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.trading.LiquidityDistribution.LiquidityCurve;
import com.nubits.nubot.utils.FilesystemUtils;
import com.nubits.nubot.utils.JSONUtils;
import com.nubits.nubot.utils.Utils;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * ParseOptions from one JSON files
 */
public class ParseOptions {

    private static final Logger LOG = LoggerFactory.getLogger(ParseOptions.class.getName());

    public static String exchangename = "exchangeName";
    public static String apikey = "apiKey";
    public static String apisecret = "apiSecret";
    public static String mailrecipient = "mailRecipient";
    public static String pair = "pair";
    public static String nudip = "nudIp";
    public static String priceincrement = "priceIncrement";
    public static String txfee = "txfee";
    public static String submitliquidity = "submitLiquidity";
    public static String executeorders = "executeOrders";
    public static String dualside = "dualSide";
    public static String verbosity = "verbosity";
    public static String gitter = "gitter";
    public static String emergencytimeout = "emergencyTimeout";
    public static String keepproceeds = "keepProceeds";
    public static String multipleoperators = "multipleOperators";
    public static String nubitaddress = "nubitAddress";
    public static String rpcpass = "rpcPass";
    public static String rpcuser = "rpcUser";
    public static String nudport = "nudPort";
    public static String mailnotifications = "mailnotifications";
    public static String mainfeed = "mainfeed";
    public static String backupfeeds = "backupFeeds";
    public static String wallchangethreshold = "wallchangeThreshold";
    public static String bypassStreaming = "bypassStreaming";
    public static String streamingserver = "streamingServer";
    public static String webport = "webport";
    public static String bookDisabletier2 = "bookDisabletier2";
    public static String booksellwall = "bookSellwall";
    public static String bookbuywall = "bookBuywall";
    public static String bookselloffset = "bookSellOffset";
    public static String bookbuyoffset = "bookBuyOffset";
    public static String booksellmaxvolumecumulative = "bookSellMaxVolumeCumulative";
    public static String bookbuymaxvolumecumulative = "bookBuyMaxVolumeCumulative";
    public static String bookbuyinterval = "bookBuyInterval";
    public static String booksellinterval = "bookSellInterval";
    public static String bookselltype = "bookSellType";
    public static String bookbuytype = "bookBuyType";
    public static String booksellsteepness = "bookSellSteepness";
    public static String bookbuysteepness = "bookBuySteepness";

    public static String poolmodeactive = "poolModeActive";
    public static String pooluri = "poolURI";
    public static String poolpayoutaddress = "poolPayoutAddress";
    public static String poolsubmitinterval = "poolSubmitInterval";

    public static String[] allkeys = {
            exchangename,
            apikey,
            apisecret,
            mailrecipient,
            pair,
            nudip,
            priceincrement,
            txfee,
            submitliquidity,
            bookbuywall,
            booksellwall,
            bookselloffset,
            bookbuyoffset,
            bookbuyinterval,
            booksellinterval,
            bookselltype,
            bookbuytype,
            booksellsteepness,
            bookbuysteepness,
            bookDisabletier2,
            executeorders,
            dualside,
            verbosity,
            gitter,
            emergencytimeout,
            keepproceeds,
            multipleoperators,
            nubitaddress,
            rpcpass,
            rpcuser,
            nudport,
            mailnotifications,
            mainfeed,
            backupfeeds,
            wallchangethreshold,
            bypassStreaming,
            streamingserver,
            webport,
            poolmodeactive,
            poolpayoutaddress,
            poolsubmitinterval,
            pooluri};
    //distributeliquidity

    private static String[] boolkeys = {submitliquidity, gitter, executeorders, dualside, multipleoperators, bypassStreaming, bookDisabletier2, poolmodeactive};


    /**
     * parse single JSON file to NuBoptions
     *
     * @param filepath
     * @return
     * @throws NuBotConfigException
     */
    public static NuBotOptions parseOptionsSingle(String filepath, boolean skipValidation) throws NuBotConfigException {
        File f = new File(filepath);
        if (!f.exists())
            throw new NuBotConfigException("file " + f.getAbsolutePath() + " does not exist");

        try {
            JSONObject inputJSON = parseSingleJsonFile(filepath);
            return parseOptionsFromJson(inputJSON, skipValidation);

        } catch (ParseException ex) {
            throw new NuBotConfigException("Parse Exception. Configuration error.");
        } catch (Exception e) {
            throw new NuBotConfigException("Configuration error : " + e);
        }

    }

    public static boolean isValidOptions(NuBotOptions options) throws NuBotConfigException {

        boolean supported = ExchangeFacade.supportedExchange(options.exchangeName);
        LOG.trace("exchange supported? " + options.exchangeName + " " + supported);
        if (!supported)
            throw new NuBotConfigException("exchange " + options.exchangeName + " not supported");

        //test if configuration is supported
        if (!CurrencyList.isPairSupported(options.getPair())) {
            throw new NuBotConfigException("The bot doesn't support yet the trading pair " + options.getPair().toString());
        }

        if (options.requiresSecondaryPegStrategy()) {
            if (!FeedFacade.isValidFeed(options.mainFeed) && options.requiresSecondaryPegStrategy())
                throw new NuBotConfigException("invalid mainfeed " + options.mainFeed);

            for (String feed : options.backupFeeds) {
                if (!FeedFacade.isValidFeed(feed) && options.requiresSecondaryPegStrategy())
                    throw new NuBotConfigException("invalid feed " + feed);
            }

            if (options.wallchangeThreshold < 0)
                throw new NuBotConfigException("invalid " + options.wallchangeThreshold);


        }

        return true;
    }

    /**
     * assign default values to optional keys not declared using NuBotOptionsDefault
     *
     * @param optionsJSON
     * @return
     */
    public static JSONObject addDefaultOptions(JSONObject optionsJSON) throws NuBotConfigException {
        NuBotOptions defaultOptions = NuBotOptionsDefault.defaultFactory(); //Assign default options

        //Put defaultOptions into a JSONobject
        JSONObject defaultOptionsJSON = null;
        try {
            defaultOptionsJSON = NuBotOptions.optionsToJSONObject(defaultOptions);
        } catch (ParseException e) {
            LOG.error(e.toString());
        }

        for (int i = 0; i < NuBotOptionsDefault.optionalKeys.length; i++) { //iterate over default keys
            if (!JSONUtils.containsIgnoreCase(optionsJSON, NuBotOptionsDefault.optionalKeys[i])) {
                String tempKey = NuBotOptionsDefault.optionalKeys[i]; //temporary key

                Object defaultValue = null;
                defaultValue = JSONUtils.getIgnoreCase(defaultOptionsJSON, tempKey);

                optionsJSON.put(tempKey, defaultValue);
            }
        }
        return optionsJSON;
    }


    /**
     * the rules for valid configurations
     *
     * @param optionsJSON
     * @return
     * @throws NuBotConfigException
     */
    public static boolean isValidJSON(JSONObject optionsJSON) throws NuBotConfigException {

        //Check if all compulsory options are defined
        for (int i = 0; i < NuBotOptionsDefault.compulsoryKeys.length; i++) {
            if (!JSONUtils.containsIgnoreCase(optionsJSON, NuBotOptionsDefault.compulsoryKeys[i]))
                throw new NuBotConfigException("necessary key: " + NuBotOptionsDefault.compulsoryKeys[i]);
        }

        for (int i = 0; i < boolkeys.length; i++) {
            try {
                boolean b = (boolean) JSONUtils.getIgnoreCase(optionsJSON, boolkeys[i]);
            } catch (Exception e) {
                throw new NuBotConfigException("can't parse to boolean: " + boolkeys[i]);
            }
        }


        try {
            String lstr = "" + JSONUtils.getIgnoreCase(optionsJSON, emergencytimeout);
            int emergencyTimeoutLong = new Integer(lstr).intValue();
        } catch (Exception e) {
            throw new NuBotConfigException("can not cast emergencytimeout to int " + e);
        }

        try {
            int nudPortlong = new Integer("" + JSONUtils.getIgnoreCase(optionsJSON, nudport)).intValue();
        } catch (Exception e) {
            throw new NuBotConfigException("can not cast nudPortlong to long " + e);
        }

        return true;
    }

    private static ArrayList parseBackupFeeds(JSONObject optionsJSON) throws NuBotConfigException {

        ArrayList backupFeeds = new ArrayList<>();

        //Iterate on backupFeeds
        JSONArray bfeeds = null;
        try {
            bfeeds = (JSONArray) JSONUtils.getIgnoreCase(optionsJSON, backupfeeds);
        } catch (Exception e) {
            throw new NuBotConfigException("can't parse array " + e);
        }

        if (bfeeds.size() < 2) {
            throw new NuBotConfigException("The bot requires at least two backup data feeds to run");
        }

        for (int i = 0; i < bfeeds.size(); i++) {
            try {
                String feedname = (String) bfeeds.get(i);
                if (!FeedFacade.isValidFeed(feedname))
                    throw new NuBotConfigException("invalid feed configured");
                else
                    backupFeeds.add(feedname);

            } catch (JSONException ex) {
                throw new NuBotConfigException("parse feeds json error" + ex);
            }
        }

        return backupFeeds;
    }

    private static String parseMails(JSONObject optionsJSON) throws NuBotConfigException {
        String tmpsendMails = (String) JSONUtils.getIgnoreCase(optionsJSON, mailnotifications);

        if (tmpsendMails.equalsIgnoreCase(MailNotifications.MAIL_LEVEL_ALL)
                || tmpsendMails.equalsIgnoreCase(MailNotifications.MAIL_LEVEL_NONE)
                || tmpsendMails.equalsIgnoreCase(MailNotifications.MAIL_LEVEL_SEVERE)) {
            return tmpsendMails.toUpperCase(); //Convert to upper case
        } else {
            String error = "Value not accepted for \"mail-notifications\" : " + tmpsendMails + " . Admitted values  : "
                    + MailNotifications.MAIL_LEVEL_ALL + " , "
                    + MailNotifications.MAIL_LEVEL_SEVERE + " or "
                    + MailNotifications.MAIL_LEVEL_NONE;
            LOG.error(error);
            throw new NuBotConfigException(error);
        }
    }

    private static String parseVerbosity(JSONObject optionsJSON) throws NuBotConfigException {
        String tmpVerbosity = (String) JSONUtils.getIgnoreCase(optionsJSON, verbosity);

        if (tmpVerbosity.equalsIgnoreCase(Constant.HIGH)
                || tmpVerbosity.equalsIgnoreCase(Constant.NORMAL)
                || tmpVerbosity.equalsIgnoreCase(Constant.LOW)) {
            return tmpVerbosity.toUpperCase(); //Convert to upper case
        } else {
            String error = "Value not accepted for \"verbosity\" : " + tmpVerbosity + " . Admitted values  : "
                    + Constant.HIGH + " , "
                    + Constant.NORMAL + " or "
                    + Constant.LOW;
            LOG.error(error);
            throw new NuBotConfigException(error);
        }
    }


    private static String parseSteepness(String input) throws NuBotConfigException {

        if (input.equalsIgnoreCase(LiquidityCurve.STEEPNESS_FLAT)
                || input.equalsIgnoreCase(LiquidityCurve.STEEPNESS_LOW)
                || input.equalsIgnoreCase(LiquidityCurve.STEEPNESS_MID)
                || input.equalsIgnoreCase(LiquidityCurve.STEEPNESS_HIGH)) {
            return input.toUpperCase(); //Convert to upper case
        } else {
            String error = "Value not accepted for \"steepness\" : " + input + " . Admitted values  : "
                    + LiquidityCurve.STEEPNESS_FLAT + " , "
                    + LiquidityCurve.STEEPNESS_LOW + " , "
                    + LiquidityCurve.STEEPNESS_MID + " or "
                    + LiquidityCurve.STEEPNESS_HIGH;
            LOG.error(error);
            throw new NuBotConfigException(error);
        }
    }


    private static String parseBookModel(String input) throws NuBotConfigException {
        if (input.equalsIgnoreCase(LiquidityCurve.TYPE_EXP)
                || input.equalsIgnoreCase(LiquidityCurve.TYPE_LOG)
                || input.equalsIgnoreCase(LiquidityCurve.TYPE_LIN)) {
            return input.toUpperCase(); //Convert to upper case
        } else {
            String error = "Value not accepted for \"booktype\" : " + input + " . Admitted values  : "
                    + LiquidityCurve.TYPE_EXP + " , "
                    + LiquidityCurve.TYPE_LOG + " or "
                    + LiquidityCurve.TYPE_LIN;
            LOG.error(error);
            throw new NuBotConfigException(error);
        }
    }

    /**
     * parseOptions from JSON into NuBotOptions
     * makes sure the parses object is valid
     *
     * @param optionsJSON
     * @return
     */
    public static NuBotOptions parseOptionsFromJson(JSONObject optionsJSON, boolean skipValidation) throws NuBotConfigException {

        try {
            isValidJSON(addDefaultOptions(optionsJSON));
        } catch (NuBotConfigException e) {
            throw e;
        }

        NuBotOptions options = new NuBotOptions();

        //Start with compulsory options
        options.exchangeName = (String) JSONUtils.getIgnoreCase(optionsJSON, exchangename);
        options.apiKey = (String) JSONUtils.getIgnoreCase(optionsJSON, apikey);
        options.apiSecret = (String) JSONUtils.getIgnoreCase(optionsJSON, apisecret);

        //Then all options that always admit a default value
        options.txFee = Utils.getDouble(JSONUtils.getIgnoreCase(optionsJSON, txfee));
        options.multipleOperators = (boolean) JSONUtils.getIgnoreCase(optionsJSON, multipleoperators);
        options.executeOrders = (boolean) JSONUtils.getIgnoreCase(optionsJSON, executeorders);
        options.gitter = (boolean) JSONUtils.getIgnoreCase(optionsJSON, gitter);
        options.mailnotifications = parseMails(optionsJSON);
        options.emergencyTimeout = new Integer("" + JSONUtils.getIgnoreCase(optionsJSON, emergencytimeout)).intValue();
        options.keepProceeds = Utils.getDouble((JSONUtils.getIgnoreCase(optionsJSON, keepproceeds)));
        options.priceIncrement = Utils.getDouble(JSONUtils.getIgnoreCase(optionsJSON, priceincrement));
        options.submitLiquidity = (boolean) JSONUtils.getIgnoreCase(optionsJSON, submitliquidity);
        options.verbosity = parseVerbosity(optionsJSON);
        options.pair = (String) JSONUtils.getIgnoreCase(optionsJSON, pair);
        options.dualSide = (boolean) JSONUtils.getIgnoreCase(optionsJSON, dualside);
        options.nudIp = (String) JSONUtils.getIgnoreCase(optionsJSON, nudip);
        options.nudPort = new Integer("" + JSONUtils.getIgnoreCase(optionsJSON, nudport)).intValue();
        options.bookDisabletier2 = (boolean) JSONUtils.getIgnoreCase(optionsJSON, bookDisabletier2);

        options.poolModeActive = (boolean) JSONUtils.getIgnoreCase(optionsJSON, poolmodeactive);


        //If port is declared via CLI, ignore config
        if (Global.webPort != -1) {
            options.webport = Global.webPort;
        } else {
            options.webport = new Integer("" + JSONUtils.getIgnoreCase(optionsJSON, webport)).intValue();
        }

        NuBotOptions defaultOptions = NuBotOptionsDefault.defaultFactory();

        //Options that can be blank if the selected pair allows if submitliuidity:false
        if (options.submitLiquidity &&
                (!JSONUtils.containsIgnoreCase(optionsJSON, nubitaddress)
                        || !JSONUtils.containsIgnoreCase(optionsJSON, rpcpass)
                        || !JSONUtils.containsIgnoreCase(optionsJSON, rpcuser))) {
            throw new NuBotConfigException("submitLiquidity is set to true and demands all RPC options to be set.");
        }

        if (!JSONUtils.containsIgnoreCase(optionsJSON, nubitaddress)) {
            options.nubitAddress = defaultOptions.getNubitAddress(); //assign default
        } else {
            options.nubitAddress = (String) JSONUtils.getIgnoreCase(optionsJSON, nubitaddress);
        }

        if (!JSONUtils.containsIgnoreCase(optionsJSON, rpcpass)) {
            options.rpcPass = defaultOptions.getRpcPass(); //assign default
        } else {
            options.rpcPass = (String) JSONUtils.getIgnoreCase(optionsJSON, rpcpass);
        }

        if (!JSONUtils.containsIgnoreCase(optionsJSON, rpcuser)) {
            options.rpcUser = defaultOptions.getRpcUser(); //assign default
        } else {
            options.rpcUser = (String) JSONUtils.getIgnoreCase(optionsJSON, rpcuser);
        }


        //Option can be blank if mailnotifications level is set to NONE
        if (!options.mailnotifications.equals(MailNotifications.MAIL_LEVEL_NONE) &&
                !JSONUtils.containsIgnoreCase(optionsJSON, mailrecipient)) {
            throw new NuBotConfigException("mailnotifications is set and demands for an email address to be set");
        }

        if (!JSONUtils.containsIgnoreCase(optionsJSON, mailrecipient)) {
            options.mailRecipient = defaultOptions.getMailRecipient(); //assign default
        } else {
            options.mailRecipient = (String) JSONUtils.getIgnoreCase(optionsJSON, mailrecipient);
        }

        //Options that can be blank if the selected pair allows it (NBT_USD)
        try {
            if (!options.requiresSecondaryPegStrategy()
                    && !JSONUtils.containsIgnoreCase(optionsJSON, mainfeed)) {
                options.mainFeed = "";
            } else {
                if (!JSONUtils.containsIgnoreCase(optionsJSON, mainfeed)) {
                    options.mainFeed = FeedFacade.getDefaultMainFeed(options.getPair());
                } else {
                    options.mainFeed = (String) JSONUtils.getIgnoreCase(optionsJSON, mainfeed);
                }
            }


            if (!options.requiresSecondaryPegStrategy()
                    && !JSONUtils.containsIgnoreCase(optionsJSON, wallchangethreshold)) {
                options.wallchangeThreshold = 0;
            } else {

                if (!JSONUtils.containsIgnoreCase(optionsJSON, wallchangethreshold)) {
                    options.wallchangeThreshold = defaultOptions.getWallchangeThreshold();
                } else {
                    options.wallchangeThreshold = Utils.getDouble(JSONUtils.getIgnoreCase(optionsJSON, wallchangethreshold));
                }
            }

            if (!options.requiresSecondaryPegStrategy()
                    && !JSONUtils.containsIgnoreCase(optionsJSON, bypassStreaming)) {
                options.bypassStreaming = false;
            } else {

                if (!JSONUtils.containsIgnoreCase(optionsJSON, bypassStreaming)) {
                    options.bypassStreaming = defaultOptions.isBypassStreaming();
                } else {
                    options.bypassStreaming = (boolean) JSONUtils.getIgnoreCase(optionsJSON, bypassStreaming);
                }
            }


            if (!options.requiresSecondaryPegStrategy()
                    && !JSONUtils.containsIgnoreCase(optionsJSON, backupfeeds)) {
                options.backupFeeds = new ArrayList<String>();
            } else {
                if (!JSONUtils.containsIgnoreCase(optionsJSON, backupfeeds)) {
                    options.backupFeeds = FeedFacade.getDefaultBackupFeeds(options.getPair());
                } else {
                    options.backupFeeds = parseBackupFeeds(optionsJSON);
                }
            }
        } catch (Exception e) {
            LOG.error(e.toString());
        }

        //if bypass stream is false, streamingserver must be set and different from ""
        if (!options.bypassStreaming) {
            LOG.warn("wallShiftThreshold and spread will be taken from Streamer service (local settings ignored)");
            if (!JSONUtils.containsIgnoreCase(optionsJSON, streamingserver)) {
                throw new NuBotConfigException("streamingserver must be set.");
            } else {
                String streamingServer = (String) JSONUtils.getIgnoreCase(optionsJSON, streamingserver);
                if (streamingServer.equals("")) {
                    streamingServer = defaultOptions.streamingserver;
                    LOG.warn("Streaming server cannot be blank. Setting it to default value: " + streamingServer);
                } else {
                    StringTokenizer st = new StringTokenizer(streamingServer, ":");
                    if (st.countTokens() != 2) { //Check if its is formatted ip:port
                        throw new NuBotConfigException("streamingserver must be in the format 'host:port'");
                    }
                }
                options.streamingserver = streamingServer;
            }
        }


        //Pool-related settings-----------------------
        String poolURI_temp = (String) JSONUtils.getIgnoreCase(optionsJSON, pooluri);
        String poolpayoutaddress_temp = (String) JSONUtils.getIgnoreCase(optionsJSON, poolpayoutaddress);

        String poolSubmitStr = "" + JSONUtils.getIgnoreCase(optionsJSON, poolsubmitinterval);
        int poolinterval_temp = new Integer(poolSubmitStr).intValue();

        if (options.poolModeActive) {
            //other pool params are required , and must be valid

            if (poolURI_temp.equals("")) {
                throw new NuBotConfigException("poolURI parameter is not set");
            } else {
                try {
                    URL testALPurl = new URL(poolURI_temp);
                    poolURI_temp = poolURI_temp.endsWith("/") ? poolURI_temp : poolURI_temp + "/";
                } catch (MalformedURLException e) {
                    throw new NuBotConfigException(pooluri + " must be in a valid URI format (http://hostname[:port])");
                }

                if (poolpayoutaddress_temp.equals("")) {
                    throw new NuBotConfigException("poolpayoutaddress_temp parameter is not set");
                }

                if (poolinterval_temp < Settings.SUBMIT_POOL_MIN_INTERVAL) {
                    throw new NuBotConfigException("poolSubmitInterval cannot be less than" + Settings.SUBMIT_POOL_MIN_INTERVAL);
                }

                //Force liquiditysubmit off

                if (options.submitLiquidity) {
                    LOG.warn("Forcing liquidity submission off : bot is set to run in pool mode.");
                    options.setSubmitLiquidity(false);
                }
            }
        }
        //Assign values
        options.poolPayoutAddress = poolpayoutaddress_temp;
        options.poolURI = poolURI_temp;
        options.poolSubmitInterval = poolinterval_temp;
        //orderbook-related settings-----------------------

        String bookSellType_temp = (String) JSONUtils.getIgnoreCase(optionsJSON, bookselltype);
        String bookBuyType_temp = (String) JSONUtils.getIgnoreCase(optionsJSON, bookbuytype);
        String bookSellSteepness_temp = (String) JSONUtils.getIgnoreCase(optionsJSON, booksellsteepness);
        String bookBuySteepness_temp = (String) JSONUtils.getIgnoreCase(optionsJSON, bookbuysteepness);
        double bookSellwall_temp = Utils.getDouble(JSONUtils.getIgnoreCase(optionsJSON, booksellwall));
        double bookBuywall_temp = Utils.getDouble(JSONUtils.getIgnoreCase(optionsJSON, bookbuywall));
        double bookSellOffset_temp = Utils.getDouble(JSONUtils.getIgnoreCase(optionsJSON, bookselloffset));
        double bookBuyOffset_temp = Utils.getDouble(JSONUtils.getIgnoreCase(optionsJSON, bookbuyoffset));
        double bookSellInterval_temp = Utils.getDouble(JSONUtils.getIgnoreCase(optionsJSON, booksellinterval));
        double bookBuyInterval_temp = Utils.getDouble(JSONUtils.getIgnoreCase(optionsJSON, bookbuyinterval));
        double bookSellMaxCumulativeVolume_temp = Utils.getDouble(JSONUtils.getIgnoreCase(optionsJSON, booksellmaxvolumecumulative));
        double bookBuyMaxCumulativeVolume_temp = Utils.getDouble(JSONUtils.getIgnoreCase(optionsJSON, bookbuymaxvolumecumulative));


        if (bookSellOffset_temp == -1) {
            try {
                bookSellOffset_temp = Utils.getDefaultOffset(options.pair);
            } catch (Exception e) {
                throw new NuBotConfigException("Problem setting default offset : " + e.toString());
            }
        }

        if (bookBuyOffset_temp == -1) {
            try {
                bookBuyOffset_temp = Utils.getDefaultOffset(options.pair);
            } catch (Exception e) {
                throw new NuBotConfigException("Problem setting default offset : " + e.toString());
            }
        }

        //Check validity of provided values
        if (bookSellwall_temp >= 0 && bookBuywall_temp >= 0) {
            options.bookSellwall = bookSellwall_temp;
            options.bookBuywall = bookBuywall_temp;
        } else {
            throw new NuBotConfigException("Wall height must be >= 0");
        }


        boolean allowLowSpreads = !options.requiresSecondaryPegStrategy();

        BigDecimal bookSellOffset_tempB = new BigDecimal(Double.toString(bookSellOffset_temp));
        BigDecimal bookBuyOffset_tempB = new BigDecimal(Double.toString(bookBuyOffset_temp));
        BigDecimal spreadTempB = bookSellOffset_tempB.add(bookBuyOffset_tempB);


        int c1 = spreadTempB.compareTo(new BigDecimal(Double.toString(Settings.MIN_SPREAD)));
        int c2 = spreadTempB.compareTo(new BigDecimal(Double.toString(Settings.MAX_SPREAD)));
        int c3 = spreadTempB.compareTo(new BigDecimal(Double.toString(Settings.MAX_SPREAD / 2)));

        /*
        Translating to Bigdecimal logic
            When 0 --> equals
            When 1 --> first value is greater than the second
            When -1 --> second value is greater than the first


            spread_temp > Settings.MIN_SPREAD  ----> c1 == 1
            spread_temp = Settings.MIN_SPREAD ----> c1 == 0
                                  >= ---> (c1 ==1 || c1 == 0)

            Old condition with doubles
            if (!((spread_temp >= Settings.MIN_SPREAD && spread_temp <= Settings.MAX_SPREAD)
                || allowLowSpreads && spread_temp <= (Settings.MAX_SPREAD) / 2)) { //for USD can be from 0 to halfmax
       */


        if (!((c1 == 1 || c1 == 0) && (c2 == -1 || c2 == 0))
                || allowLowSpreads && (c3 == -1 || c3 == 0)) {

            String message = "\n\n\nThe total spread you set (" + (spreadTempB.toString()) + "$) is out of the " +
                    "allowed range (" + Settings.MIN_SPREAD + "," + Settings.MAX_SPREAD + "). \n" +
                    "We highly reccomend to adjust your offset so that your values are within that boundary." +
                    "See motion 0ec0be7f113a0bf6ff603545a974cd6410458e00 for more details\n\n\n";

            LOG.error(message);
        }
        options.bookSellOffset = bookSellOffset_temp;
        options.bookBuyOffset = bookBuyOffset_temp;

        double minIntervalAllowed = 0.000001;
        double maxIntervalAllowed = 0.3;
        if ((bookSellInterval_temp > minIntervalAllowed && bookSellInterval_temp < maxIntervalAllowed)
                && (bookBuyInterval_temp > minIntervalAllowed && bookBuyInterval_temp < maxIntervalAllowed)) {
            options.bookSellInterval = bookSellInterval_temp;
            options.bookBuyInterval = bookBuyInterval_temp;
        } else {
            throw new NuBotConfigException("book interval should be greater than " + minIntervalAllowed + "$ and less than " + maxIntervalAllowed + "$");
        }


        if (bookBuyMaxCumulativeVolume_temp >= 0) {
            options.bookBuyMaxVolumeCumulative = bookBuyMaxCumulativeVolume_temp;
        } else {
            throw new NuBotConfigException("bookBuyMaxVolumeCumulative must be > 0");
        }

        if (bookSellMaxCumulativeVolume_temp >= 0) {
            options.bookSellMaxVolumeCumulative = bookSellMaxCumulativeVolume_temp;
        } else {
            throw new NuBotConfigException("bookSellMaxVolumeCumulative must be > 0");
        }


        options.bookSellType = parseBookModel(bookSellType_temp);
        options.bookBuyType = parseBookModel(bookBuyType_temp);
        options.bookSellSteepness = parseSteepness(bookSellSteepness_temp);
        options.bookBuySteepness = parseSteepness(bookBuySteepness_temp);

        if (!skipValidation) {
            try {
                isValidOptions(options);
            } catch (NuBotConfigException e) {
                throw e;
            }
        } else {
            LOG.debug("Skipping validation of configuration file.");
        }

        return options;
    }

    public static NuBotOptions parsePost(JSONObject postJson, boolean skipValidation) throws Exception {

        NuBotOptions newopt = null;

        try {
            //Check if NuBot has valid parameters
            newopt = ParseOptions.parseOptionsFromJson(postJson, skipValidation);
            LOG.debug("parse post opt: " + newopt);

        } catch (NuBotConfigException e) {
            throw e;

        }

        return newopt;
    }


    /**
     * parse single json file
     *
     * @param filepath
     * @return
     * @throws ParseException
     */

    public static JSONObject parseSingleJsonFile(String filepath) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject fileJSON = (JSONObject) (parser.parse(FilesystemUtils.readFromFile(filepath)));
        return fileJSON;
    }


}
