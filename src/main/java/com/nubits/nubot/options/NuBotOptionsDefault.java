/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.options;

import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.notifications.MailNotifications;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.trading.LiquidityDistribution.LiquidityCurve;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default options for NuBot
 */
public class NuBotOptionsDefault {
    private static final Logger LOG = LoggerFactory.getLogger(NuBotOptionsDefault.class.getName());

    public static String[] optionalKeys = {
            ParseOptions.txfee,
            ParseOptions.multipleoperators,
            ParseOptions.executeorders,
            ParseOptions.mailnotifications,
            ParseOptions.emergencytimeout,
            ParseOptions.keepproceeds,
            ParseOptions.gitter,
            ParseOptions.priceincrement,
            ParseOptions.submitliquidity,
            ParseOptions.verbosity,
            ParseOptions.webport,
            ParseOptions.nudip,
            ParseOptions.nudport,
            ParseOptions.pair,
            ParseOptions.dualside,
            ParseOptions.bypassStreaming,
            ParseOptions.streamingserver,
            ParseOptions.booksellwall,
            ParseOptions.bookbuywall,
            ParseOptions.bookDisabletier2,
            ParseOptions.bookselloffset,
            ParseOptions.bookbuyoffset,
            ParseOptions.bookbuyinterval,
            ParseOptions.booksellinterval,
            ParseOptions.booksellmaxvolumecumulative,
            ParseOptions.bookbuymaxvolumecumulative,
            ParseOptions.bookselltype,
            ParseOptions.bookbuytype,
            ParseOptions.booksellsteepness,
            ParseOptions.bookbuysteepness,
            ParseOptions.poolmodeactive,
            ParseOptions.pooluri,
            ParseOptions.poolsubmitinterval,
            ParseOptions.poolpayoutaddress
    };

    public static String[] compulsoryKeys = {ParseOptions.apikey, ParseOptions.exchangename, ParseOptions.apisecret};


    public static NuBotOptions defaultFactory() {


        NuBotOptions opt = new NuBotOptions();

        //Compulsory options (+)Remember to update compulsoryKeys array above
        opt.apiKey = "";
        opt.exchangeName = "";
        opt.apiSecret = "";

        //Default options. (+)Remember to update optionalKeys array above
        opt.txFee = 0.2;
        opt.multipleOperators = false;
        opt.executeOrders = true;
        opt.gitter = true;
        opt.mailnotifications = MailNotifications.MAIL_LEVEL_SEVERE;
        opt.emergencyTimeout = 60;
        opt.keepProceeds = 0.0;
        opt.priceIncrement = 0.0003;
        opt.submitLiquidity = false;
        opt.verbosity = "normal";
        CurrencyPair defaultPair = CurrencyList.NBT_BTC;
        opt.pair = defaultPair.toStringSep();
        opt.dualSide = true;
        opt.nudIp = "127.0.0.1"; //Can be left blank if submitliuidity:false
        opt.nudPort = 9091;//Can be left blank if submitliuidity:false
        opt.streamingserver = "stream.tradingbot.nu:5556";
        opt.webport = Settings.WEB_DEFAULT_PORT;

        opt.poolModeActive = false;
        opt.poolSubmitInterval = 50;
        opt.poolPayoutAddress = ""; //Can be left blank if poolModeActive:false
        opt.poolURI = "";  //Can be left blank if poolModeActive:false

        //Orderbook related settings
        opt.bookDisabletier2 = false;

        opt.bookSellwall = 500;
        opt.bookBuywall = 500;

        opt.bookSellInterval = 0.008;
        opt.bookBuyInterval = 0.008;

        opt.bookSellMaxVolumeCumulative = 0;
        opt.bookBuyMaxVolumeCumulative = 0;


        //if not set, put them to null or -1, the ParseOption will take care
        opt.bookSellOffset = -1;
        opt.bookBuyOffset = -1;

        opt.bookSellType = LiquidityCurve.TYPE_EXP;
        opt.bookBuyType = LiquidityCurve.TYPE_EXP;

        opt.bookSellSteepness = LiquidityCurve.STEEPNESS_MID;
        opt.bookBuySteepness = LiquidityCurve.STEEPNESS_MID;

        //Options that can be blank if submitliuidity:false
        opt.nubitAddress = "";
        opt.rpcPass = "";
        opt.rpcUser = "";

        //Option can be blank if mailnotifications level is set to NONE
        opt.mailRecipient = "";

        //Options that can be blank if the selected pair allows it (NBT_USD) ,
        //otherwise ther value must depend from pegged pair
        opt.wallchangeThreshold = Settings.DEFAULT_WALLSHIFT_TRESHOLD;
        opt.bypassStreaming = false;

        try {
            opt.mainFeed = FeedFacade.getDefaultMainFeed(defaultPair);
            opt.backupFeeds = FeedFacade.getDefaultBackupFeeds(defaultPair);
        } catch (Exception e) {
            LOG.error(e.toString());
        }

        return opt;
    }
}
