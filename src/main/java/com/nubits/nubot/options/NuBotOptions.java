/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.options;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.notifications.MailNotifications;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class NuBotOptions {

    private static final Logger LOG = LoggerFactory.getLogger(NuBotOptions.class.getName());

    /**
     * Custodian's public key to access the exchange
     */
    public String apiKey;

    /**
     * Custodian's secret key to access the exchange
     */
    public String apiSecret;

    /**
     * the email to which emergency email are sent
     */
    public String mailRecipient;

    /**
     * Name of the exchange where the bots operates
     */
    public String exchangeName;

    /**
     * If set to true, the bot will behave as a dual side custodian, if false as a sell side custodian.
     */
    public boolean dualSide;

    /**
     * valid currency pair for the specified eg. "nbt_usd"
     */
    public String pair;

    //Conditional settings with a default value

    /**
     * The RPC username of the Nu daemon
     */
    public String rpcUser;

    /**
     * The RPC password of the Nu daemon
     */
    public String rpcPass;

    /**
     * The public address where the custodial grant has been received
     */
    public String nubitAddress;

    /**
     * The RPC port of the Nu daemon
     */
    public int nudPort;

    //Optional settings with a default value  ----------------------------

    /**
     * flag to skip Streaming Service if se to true
     */
    public boolean bypassStreaming;

    /**
     * Server address for Streaming Service
     */
    public String streamingserver;

    /**
     * The IP address of the machine that hosts the Nu Client
     */
    public String nudIp;

    /**
     * if set to false will disable email notifications
     */
    public String mailnotifications;

    /**
     * if set to false, the bot will not try to submit liquidity info.
     * If set to false, it will also allow the custodian to omit the declaration of nubitaddress , nudport , rpcuser and rpcpass
     */
    public boolean submitLiquidity;

    /**
     * if set to false the bot will print a warning instead of executing orders
     */
    public boolean executeOrders;

    /**
     * high : ( log.level >= debug )
     * normal : ( log.level >= info )
     * low : ( log.level >= warning )
     */
    public String verbosity;

    /**
     * if set to false will disable gitter notifications
     */
    public boolean gitter;

    /**
     * if set to true, will sync with remote NPT and reset orders often
     */
    public boolean multipleOperators;

    /**
     * If transaction fee not available from the exchange via api, this value will be used
     */
    public double txFee;

    /**
     * if working in sell-side mode, this value (considered USD) will be added to the sell price
     */
    public double priceIncrement;

    /**
     * max amount of minutes of consecutive failure. After those minute elapse, emergency procedure starts
     */
    public int emergencyTimeout;

    /**
     * Specific setting for KTm's proposal. Will keep the specified proceeds from sales apart instead of putting 100% of balance on buy .
     */
    public double keepProceeds;


    public double wallchangeThreshold;

    public int webport;


    //Parametric order book related parameters ---- see docs    -----------------------------------
    /**
     * When this flag is true, the bot will put tier2 on orderbook at different price levels
     */
    public boolean bookDisabletier2;


    /**
     * maximum volume to put on walls at the best price (tier1).
     */
    public double bookSellwall;
    public double bookBuywall;

    /**
     * offset from target price, expressed in USD
     */
    public double bookSellOffset;
    public double bookBuyOffset;

    /**
     * interval price between two orders (expressed in USD).
     */
    public double bookSellMaxVolumeCumulative;
    public double bookBuyMaxVolumeCumulative;

    /**
     * cumulative maximums size of orders (expressed in USD).
     */
    public double bookBuyInterval;
    public double bookSellInterval;


    /**
     * type of order book, see LiquidityCurve.java for types available
     */
    public String bookSellType;
    public String bookBuyType;

    /**
     * steepness of the LiquidityCurve [low,mid,high,flat]
     */
    public String bookSellSteepness;
    public String bookBuySteepness;

    // pools ---------------------------------------------------------------------------------------

    /**
     * if set to true will make NuBot behave as a client of liquidity pool
     */
    public boolean poolModeActive;


    /**
     * URI of the pool
     */
    public String poolURI;


    /**
     * NBT address where you wish to receive the payout
     */
    public String poolPayoutAddress;


    /**
     * Interval expressed in seconds between submissions to liquidity pool server
     */
    public int poolSubmitInterval;

    // feeds ---------------------------------------------------------------------------------------

    public String mainFeed;
    public ArrayList<String> backupFeeds;

    /**
     * empty constructor. assumes safe creation of valid options
     */
    public NuBotOptions() {
        backupFeeds = new ArrayList<>();
    }

    public static String optionsToJSONString(NuBotOptions opt) {
        GsonBuilder gson = new GsonBuilder().setPrettyPrinting();
        //gson.registerTypeAdapter(NuBotOptions.class, new NuBotOptionsSerializer());
        Gson parser = gson.create();
        String js = parser.toJson(opt);
        return js;

    }

    public static JSONObject optionsToJSONObject(NuBotOptions opt) throws ParseException {
        GsonBuilder gson = new GsonBuilder().setPrettyPrinting();
        //gson.registerTypeAdapter(NuBotOptions.class, new NuBotOptionsSerializer());
        Gson parser = gson.create();
        String js = parser.toJson(opt);

        JSONObject obj = (JSONObject) (new JSONParser().parse(js));

        return obj;
    }


    public boolean requiresSecondaryPegStrategy() {
        //Return TRUE when it requires a dedicated NBT peg to something that is not USD
        if (this.pair.equalsIgnoreCase(CurrencyList.NBT_USD.toStringSep())) {
            return false;
        } else {
            return true;
        }
    }


    @Override
    public String toString() {
        return toStringNoKeys(); //removes sensitive information
    }


    private String toStringNoKeys() {
        String toRet = "";

        GsonBuilder gson = new GsonBuilder().setPrettyPrinting();
        gson.setLongSerializationPolicy(LongSerializationPolicy.STRING);
        //gson.registerTypeAdapter(NuBotOptions.class, new NuBotOptionsSerializer());
        /*gson.registerTypeAdapter(Double.class, new JsonSerializer<Double>() {
            @Override
            public JsonElement serialize(final Double src, final Type typeOfSrc, final JsonSerializationContext context) {
                BigDecimal value = BigDecimal.valueOf(src);

                return new JsonPrimitive(value);
            }
        });*/
        Gson parser = gson.create();

        String serializedOptionsStr = parser.toJson(this);

        org.json.simple.parser.JSONParser p = new org.json.simple.parser.JSONParser();
        try {
            JSONObject serializedOptionsJSON = (JSONObject) (p.parse(serializedOptionsStr));

            //Replace sensitive information
            String[] sensitiveKeys = {"apisecret", "apikey", "rpcpass", "apiSecret", "apiKey", "rpcPass"};
            String replaceString = "hidden";

            for (int i = 0; i < sensitiveKeys.length; i++) {
                if (serializedOptionsJSON.containsKey(sensitiveKeys[i])) {
                    serializedOptionsJSON.replace(sensitiveKeys[i], replaceString);
                }
            }

            toRet = serializedOptionsJSON.toString();
        } catch (org.json.simple.parser.ParseException e) {
            LOG.error(e.toString());
        }

        return toRet;
    }


    /* --------------------------- Accessors -----------------------------------------*/

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiSecret() {
        return apiSecret;
    }

    public void setApiSecret(String apiSecret) {
        this.apiSecret = apiSecret;
    }

    public String getMailRecipient() {
        return mailRecipient;
    }

    public void setMailRecipient(String mailRecipient) {
        this.mailRecipient = mailRecipient;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public boolean isDualSide() {
        return dualSide;
    }

    public void setDualSide(boolean dualSide) {
        this.dualSide = dualSide;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public void setPair(CurrencyPair pair) {
        this.pair = pair.toStringSep();
    }

    public String getRpcUser() {
        return rpcUser;
    }

    public void setRpcUser(String rpcUser) {
        this.rpcUser = rpcUser;
    }

    public String getRpcPass() {
        return rpcPass;
    }

    public void setRpcPass(String rpcPass) {
        this.rpcPass = rpcPass;
    }

    public String getNubitAddress() {
        return nubitAddress;
    }

    public void setNubitAddress(String nubitAddress) {
        this.nubitAddress = nubitAddress;
    }

    public int getNudPort() {
        return nudPort;
    }

    public void setNudPort(int nudPort) {
        this.nudPort = nudPort;
    }

    public boolean isBypassStreaming() {
        return bypassStreaming;
    }

    public void setBypassStreaming(boolean bypassStreaming) {
        this.bypassStreaming = bypassStreaming;
    }

    public String getStreamingserver() {
        return streamingserver;
    }

    public void setStreamingserver(String streamingserver) {
        this.streamingserver = streamingserver;
    }

    public String getNudIp() {
        return nudIp;
    }

    public void setNudIp(String nudIp) {
        this.nudIp = nudIp;
    }

    public String getSendMailsLevel() {
        return mailnotifications;
    }

    public void setMailnotifications(String mailnotifications) {
        this.mailnotifications = mailnotifications;
    }

    public boolean isSubmitLiquidity() {
        return submitLiquidity;
    }

    public void setSubmitLiquidity(boolean submitLiquidity) {
        this.submitLiquidity = submitLiquidity;
    }

    public boolean isExecuteOrders() {
        return executeOrders;
    }

    public void setExecuteOrders(boolean executeOrders) {
        this.executeOrders = executeOrders;
    }

    public String getVerbosity() {
        return verbosity;
    }

    public void setVerbosity(String verbosity) {
        this.verbosity = verbosity;
    }

    public boolean isGitter() {
        return gitter;
    }

    public void setGitter(boolean gitter) {
        this.gitter = gitter;
    }

    public boolean isMultipleOperators() {
        return multipleOperators;
    }

    public void setMultipleOperators(boolean multipleOperators) {
        this.multipleOperators = multipleOperators;
    }

    public double getTxFee() {
        return txFee;
    }

    public void setTxFee(double txFee) {
        this.txFee = txFee;
    }

    public double getPriceIncrement() {
        return priceIncrement;
    }

    public void setPriceIncrement(double priceIncrement) {
        this.priceIncrement = priceIncrement;
    }

    public int getEmergencyTimeout() {
        return emergencyTimeout;
    }

    public void setEmergencyTimeout(int emergencyTimeout) {
        this.emergencyTimeout = emergencyTimeout;
    }

    public double getKeepProceeds() {
        return keepProceeds;
    }

    public void setKeepProceeds(double keepProceeds) {
        this.keepProceeds = keepProceeds;
    }

    public double getWallchangeThreshold() {
        return wallchangeThreshold;
    }

    public void setWallchangeThreshold(double wallchangeThreshold) {
        this.wallchangeThreshold = wallchangeThreshold;
    }

    public int getWebport() {
        return webport;
    }

    public void setWebport(int webport) {
        this.webport = webport;
    }

    public boolean isBookDisabletier2() {
        return bookDisabletier2;
    }

    public void setBookDisabletier2(boolean bookDisabletier2) {
        this.bookDisabletier2 = bookDisabletier2;
    }

    public double getBookSellwall() {
        return bookSellwall;
    }

    public void setBookSellwall(double bookSellwall) {
        this.bookSellwall = bookSellwall;
    }

    public double getBookBuywall() {
        return bookBuywall;
    }

    public void setBookBuywall(double bookBuywall) {
        this.bookBuywall = bookBuywall;
    }

    public double getBookSellOffset() {
        return bookSellOffset;
    }

    public void setBookSellOffset(double bookSellOffset) {
        this.bookSellOffset = bookSellOffset;
    }

    public double getBookBuyOffset() {
        return bookBuyOffset;
    }

    public void setBookBuyOffset(double bookBuyOffset) {
        this.bookBuyOffset = bookBuyOffset;
    }

    public double getBookBuyInterval() {
        return bookBuyInterval;
    }

    public void setBookBuyInterval(double bookBuyInterval) {
        this.bookBuyInterval = bookBuyInterval;
    }

    public double getBookSellInterval() {
        return bookSellInterval;
    }

    public void setBookSellInterval(double bookSellInterval) {
        this.bookSellInterval = bookSellInterval;
    }

    public String getBookSellType() {
        return bookSellType;
    }

    public void setBookSellType(String bookSellType) {
        this.bookSellType = bookSellType;
    }

    public String getBookBuyType() {
        return bookBuyType;
    }

    public void setBookBuyType(String bookBuyType) {
        this.bookBuyType = bookBuyType;
    }

    public String getBookSellSteepness() {
        return bookSellSteepness;
    }

    public void setBookSellSteepness(String bookSellSteepness) {
        this.bookSellSteepness = bookSellSteepness;
    }

    public String getBookBuySteepness() {
        return bookBuySteepness;
    }

    public void setBookBuySteepness(String bookBuySteepness) {
        this.bookBuySteepness = bookBuySteepness;
    }

    public String getMainFeed() {
        return mainFeed;
    }

    public void setMainFeed(String mainFeed) {
        this.mainFeed = mainFeed;
    }

    public double getBookSellMaxVolumeCumulative() {
        return bookSellMaxVolumeCumulative;
    }

    public void setBookSellMaxVolumeCumulative(double bookSellMaxVolumeCumulative) {
        this.bookSellMaxVolumeCumulative = bookSellMaxVolumeCumulative;
    }

    public boolean isPoolModeActive() {
        return poolModeActive;
    }

    public void setPoolModeActive(boolean poolModeActive) {
        this.poolModeActive = poolModeActive;
    }

    public String getPoolURI() {
        return poolURI;
    }

    public void setPoolURI(String poolURI) {
        this.poolURI = poolURI;
    }

    public String getPoolPayoutAddress() {
        return poolPayoutAddress;
    }

    public void setPoolPayoutAddress(String poolPayoutAddress) {
        this.poolPayoutAddress = poolPayoutAddress;
    }

    public int getPoolSubmitInterval() {
        return poolSubmitInterval;
    }

    public void setPoolSubmitInterval(int poolSubmitInterval) {
        this.poolSubmitInterval = poolSubmitInterval;
    }

    public double getBookBuyMaxVolumeCumulative() {
        return bookBuyMaxVolumeCumulative;
    }

    public void setBookBuyMaxVolumeCumulative(double bookBuyMaxVolumeCumulative) {
        this.bookBuyMaxVolumeCumulative = bookBuyMaxVolumeCumulative;
    }

    public ArrayList<String> getBackupFeeds() {
        return backupFeeds;
    }

    public void setBackupFeeds(ArrayList<String> backupFeeds) {
        this.backupFeeds = backupFeeds;
    }

    public CurrencyPair getPair() {
        CurrencyPair pair = CurrencyPair.getCurrencyPairFromString(this.pair);
        return pair;
    }

    public boolean sendMails() {
        boolean not_none = !(this.mailnotifications.equals(MailNotifications.MAIL_LEVEL_NONE));
        return not_none;
    }

}


