/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package com.nubits.nubotstream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A shiftOrders message
 */
public class ShiftOrdersMessage extends AbstractStreamMessage {

    final static Logger LOG = LoggerFactory.getLogger(ShiftOrdersMessage.class);

    public ShiftOrdersMessage() { //needed by gson deseriaization
    }

    public ShiftOrdersMessage(String[] commandsArgs, String error, Object[] attachments, String serverTimestamp, String token) {
        super();
        command = StreamMessageFacade.COMMAND_SHIFT_WALLS;
        this.args = parseArgs(commandsArgs);

        this.errorMessage = error;
        this.attachments = attachments;
        this.serverTimestamp = serverTimestamp;
        this.token = token;

    }

    @Override
    String[] parseArgs(String[] args) {
        int numberOfArgsCorrect = 9;
        if (args.length == numberOfArgsCorrect) {
            /*
                args[0] = pair
                args[1] = midprice
                args[2] = suggestedSellPriceUSD
                args[3] = suggestedBuyPriceUSD
                args[4] = suggestedSellPricePEG
                args[5] = suggestedBuyPricePEG
                args[6] = pricesource
                args[7] = shiftThreshold
                args[8] = spread
             */
        } else {
            LOG.error("Wrong number or args : you provided " + args.length + " instead of " + numberOfArgsCorrect);
        }

        return args;
    }

    public static ShiftOrdersMessage parseFromString(String json) {
        Gson parser = new GsonBuilder().setPrettyPrinting().create();
        ShiftOrdersMessage value = parser.fromJson(json, ShiftOrdersMessage.class);
        return value;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

