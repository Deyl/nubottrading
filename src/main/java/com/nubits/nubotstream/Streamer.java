package com.nubits.nubotstream;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.CredentialManager;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.launch.MainLaunch;
import com.nubits.nubot.models.Currency;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.notifications.MailNotifications;
import com.nubits.nubot.pricefeeds.PriceFeedManager;
import com.nubits.nubot.utils.TimeUtils;
import com.nubits.nubot.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Streamer
 * streams data from exchanges
 * publishes commands to subscribed nubots
 */
public class Streamer {

    static {
        System.setProperty("logback.configurationFile", Settings.STREAM_LOGXML);
    }

    private static final Logger LOG = LoggerFactory.getLogger(Streamer.class.getName());

    private static Set<String> totalConnectedClients;

    private static ArrayList<StreamerCurrencyTracker> trackerList;

    private static String pingport; //port for ping requests
    private static String mainport; //port for ping requests

    private static boolean sendmail;
    private static String mailRecp;

    private static double spreadHighVolatility;
    private static double spreadLowVolatility;

    private static double tresholdHighVolatility;
    private static double tresholdLowVolatility;

    private static String[] currencies;

    public static void initStreamer() {
        //Try to setup the access manager
        try {
            Global.credentialManager = CredentialManager.createCredentialManager(false);
        } catch (Exception e) {
            LOG.error(e.toString());
            MainLaunch.exitWithNotice("Problems while creating CredentialManager.");
        }


        LOG.info("Starting Streamer");
        readProp();

        //Try to setup the access manager
        try {
            Global.credentialManager = CredentialManager.createCredentialManager(false);
        } catch (Exception e) {
            LOG.error(e.toString());
        }

        if (sendmail) {
            MailNotifications.sendFromStreamer(mailRecp, "Streamer started", "Streamer started at " + TimeUtils.GetUTCdatetimeAsString());
        }

        totalConnectedClients = new HashSet<>();

        //setup ZMQ context
        ZMQ.Context context = ZMQ.context(1);


        //Initialize all price trackers
        trackerList = new ArrayList<>();


        LOG.warn("\nPorts Map : ----------- pingport:" + pingport + "mainport:" + mainport);
        int initialCurrencyPort = 5561;


        LOG.info("Setting up trackers for " + currencies.length + " currencies");

        for (int i = 0; i < currencies.length; i++) {
            String tempCurrencyCode = currencies[i];
            Currency tempCurrency = Currency.createCurrency(tempCurrencyCode);
            String tempToken = generateToken(tempCurrency);
            int tempCurrencyPort = initialCurrencyPort + i;

            long tempFetchInterval = Settings.FEEDFETCH_DEFAULT_INTERVAL;
            if (tempCurrency.isFiat()) {
                tempFetchInterval = Settings.FEEDFETCH_FIAT_INTERVAL;
            }
            double spread = getSpread(tempCurrency);
            double shiftTreshold = getTreshold(tempCurrency);
            trackerList.add(new StreamerCurrencyTracker(tempCurrency, tempCurrencyPort, tempToken, spread, shiftTreshold, tempFetchInterval, context));
            LOG.warn("Adding tracker : [" + tempCurrencyCode + "] :  feedPort:" + tempCurrencyPort + ", [" + tempCurrencyCode + "] " +
                    "firstTimePort:" + (tempCurrencyPort + 100) + ", spread:" + spread + "%," +
                    "treshold: " + shiftTreshold + "%, token:" + tempToken);
        }

        LOG.warn("\n\n");

        startAllTrackers();

        //setup replier
        startMainReplier(context);
        startPingReplier(context);

    }

    private static double getSpread(Currency currency) {
        if (currency.equals(CurrencyList.USD)) {
            return 0;
        } else if (currency.isFiat()) {
            return spreadLowVolatility;
        } else {
            return spreadHighVolatility;
        }
    }

    private static double getTreshold(Currency currency) {
        if (currency.equals(CurrencyList.USD)) {
            return 0;
        } else if (currency.isFiat()) {
            return tresholdLowVolatility;
        } else {
            return tresholdHighVolatility;
        }
    }

    private static void startMainReplier(ZMQ.Context context) {
        String connectionStringRep = "tcp://*:" + mainport;
        LOG.warn("Opening main socket to on : " + connectionStringRep);
        ZMQ.Socket replier = context.socket(ZMQ.REP);
        replier.bind(connectionStringRep);

        //Create a thread which will respond to the requests
        Thread t = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    byte[] req = replier.recv(0);
                    String sreq = new String(req, ZMQ.CHARSET);
                    //Message Format : id token currencycode
                    LOG.debug("Received " + sreq);
                    StringTokenizer st = new StringTokenizer(sreq);
                    int portN = -1;
                    String currencyToken = "";
                    String error = "";
                    String id = "";
                    //Check the message format
                    if (st.countTokens() == 3) {
                        id = (String) st.nextElement();
                        String token = (String) st.nextElement();
                        String currencyCode = (String) st.nextElement();

                        //Check the auth token
                        if (token.equals(Global.credentialManager.get(CredentialManager.STREAMER_TOKEN_KEY))
                                || token.equals(Global.credentialManager.get(CredentialManager.LIQUIDITY_PROVIDER_KEY))) {
                            LOG.info("Token is valid. client authorized : " + id);

                            //Get the socket port for the requested currency

                            try {
                                portN = getSocketPort(currencyCode);
                                currencyToken = getCurrencyToken(currencyCode);
                            } catch (Exception e) {
                                LOG.warn("A client (" + id + ") with a valid token requested an invalid currency: " + currencyCode);
                                error = e.getMessage();
                            }

                            //Check if a client with the same id is already connected
                            if (!totalConnectedClients.contains(id)) {
                                totalConnectedClients.add(sreq);
                                LOG.debug("new bot requested conection " + sreq);
                                LOG.debug("Cumulative connection requests from bots : " + totalConnectedClients.size());
                            } else {
                                LOG.warn("A client (" + id + " is trying to connect again");
                                error = "You are already connected with sessionID=" + id + ". Only one connection per session accepted";
                            }

                        } else {
                            error = "Wrong token";
                            LOG.warn("Wrong token received. client un-authorized : " + id);
                        }
                    } else {
                        LOG.warn("Wrong message format received:" + sreq);
                        error = "Wrong message format :" + sreq;
                    }

                    String[] args;
                    if (error.equals("")) {
                        //Prepare the args
                        args = new String[2];
                        args[0] = Integer.toString(portN);
                        args[1] = currencyToken;
                    } else {
                        args = new String[0];
                    }
                    Object[] attachments = new Object[0];
                    String serverTime = TimeUtils.GetUTCdatetimeAsString();
                    String token = "";

                    InitSocketMessage ism = new InitSocketMessage(args, error, attachments, serverTime, token);
                    String messageString = ism.toString();

                    LOG.debug("Replying to [" + id + "] with : " + messageString);
                    replier.send(messageString.getBytes(ZMQ.CHARSET), 0);

                }
            }
        });
        t.start();
    }

    private static void startPingReplier(ZMQ.Context context) {
        String connectionStringRep = "tcp://*:" + pingport;
        LOG.warn("Opening socket to reply to ping requests : " + connectionStringRep);
        ZMQ.Socket replier = context.socket(ZMQ.REP);
        replier.bind(connectionStringRep);
        //start a thread which will respond to the requests about the last message
        Thread t = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    byte[] req = replier.recv(0);
                    String sreq = new String(req, ZMQ.CHARSET);
                    LOG.trace("Received " + sreq);
                    if (sreq.equals("ping?")) { //See if the message is ping? reply with a pong?
                        LOG.debug("Received a ping request. Responding with a pong!");
                        String rep = "pong!";
                        replier.send(rep.getBytes(ZMQ.CHARSET), 0);
                    }
                }
            }
        });
        t.start();
    }

    public static void main(String[] args) {
        initStreamer();
    }


    private static String generateToken(Currency currency) {
        String uid = UUID.randomUUID().toString().substring(0, 6);
        return currency.getCode() + "_" + uid;
    }

    private static void startAllTrackers() {
        ScheduledExecutorService scheduler =
                Executors.newScheduledThreadPool(trackerList.size());

        for (int i = 0; i < trackerList.size(); i++) {
            StreamerCurrencyTracker tracker = trackerList.get(i);
            final ScheduledFuture<?> taskHandle =
                    scheduler.schedule(tracker, i * 30, SECONDS); //Each task should start after 30 seconds from the previous
        }
    }

    private static int getSocketPort(String currencyCode) throws Exception {
        boolean found = false;
        for (StreamerCurrencyTracker tracker : trackerList) {
            if (tracker.getCurrency().getCode().equalsIgnoreCase(currencyCode)) {
                return tracker.getSocketPort();
            }
        }
        if (!found) {
            throw new Exception("Cannot find a socket for " + currencyCode);
        }
        return -1; //never reached
    }


    private static String getCurrencyToken(String currencyCode) throws Exception {
        boolean found = false;
        for (StreamerCurrencyTracker tracker : trackerList) {
            if (tracker.getCurrency().getCode().equalsIgnoreCase(currencyCode)) {
                return tracker.getToken();
            }
        }
        if (!found) {
            throw new Exception("Cannot find a token for " + currencyCode);
        }
        return ""; //never reached
    }


    private static PriceFeedManager getPriceFeedManager(Currency currency) throws Exception {
        boolean found = false;
        for (StreamerCurrencyTracker tracker : trackerList) {
            if (tracker.getCurrency().getCode().equalsIgnoreCase(currency.getCode())) {
                return tracker.getPricefeedManager();
            }
        }
        if (!found) {
            throw new Exception("Cannot find a socket for " + currency.getCode());
        }
        return null; //never reached
    }

    /**
     * read the streamer properties from the file
     */
    private static void readProp() {
        try {
            FileReader reader = new FileReader(Settings.STREAMER_PROP_FILE);
            Properties properties = new Properties();
            properties.load(reader);

            mainport = properties.getProperty("mainport");
            int intmainport = Integer.parseInt(mainport);
            pingport = Integer.toString(intmainport - 1); //pingport = mainport-1
            sendmail = new Boolean(properties.getProperty("sendmail")).booleanValue();
            mailRecp = properties.getProperty("mailRecp");
            spreadHighVolatility = Utils.getDouble(properties.getProperty("spread-highvolatility"));
            spreadLowVolatility = Utils.getDouble(properties.getProperty("spread-lowvolatility"));
            tresholdHighVolatility = Utils.getDouble(properties.getProperty("treshold-highvolatility"));
            tresholdLowVolatility = Utils.getDouble(properties.getProperty("treshold-lowvolatility"));
            //Parse the list of currencies to track
            String currenciesString = properties.getProperty("track");

            StringTokenizer st = new StringTokenizer(currenciesString, ",");
            if (st.countTokens() != 0) {
                currencies = new String[st.countTokens()];
                int i = 0;
                while (st.hasMoreElements()) {
                    String tempCurrencyCode = st.nextElement().toString();
                    LOG.info("Requested to track " + tempCurrencyCode);
                    currencies[i] = tempCurrencyCode;
                    i++;
                }

            } else {
                LOG.error("No currencies found to track. If you want to track one currency, use 'track = XXX,' with a comma at the end.");
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
