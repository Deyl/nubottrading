/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubotstream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nubits.nubot.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A message object to send data between publisher and subscriber
 */

 /*
        Message format
          "command": "cmdString",
          "commandArgs": [
            "arg1",
            "arg2"
          ],
          "error": "error description. Empty string if ok",
          "attachments": {},
          "serverTimestamp": 21231232123,
          "token": ""

 */


public abstract class AbstractStreamMessage {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractStreamMessage.class.getName());

    protected String command;
    protected Object[] args;

    protected String errorMessage;

    protected String token;
    protected String serverTimestamp;

    protected Object[] attachments;

    public String toString() {
        Gson parser = new GsonBuilder().setPrettyPrinting().create();
        String serializedMsg = parser.toJson(this);
        serializedMsg = Utils.removeAllNewlines(serializedMsg).trim();
        return serializedMsg;
    }

    abstract String[] parseArgs(String[] args);

    public boolean isError() {
        return (!getErrorMessage().equals(""));
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] commandArgs) {
        this.args = commandArgs;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getServerTimestamp() {
        return serverTimestamp;
    }

    public void setServerTimestamp(String serverTimestamp) {
        this.serverTimestamp = serverTimestamp;
    }

    public Object[] getAttachments() {
        return attachments;
    }

    public void setAttachments(Object[] attachments) {
        this.attachments = attachments;
    }
}

