##Available price feed names (can be used as value for *main-feed* and *backup-feeds*)

For default values check currencies.csv

| Feed name        | Currencies available for tracking    |  Feed |   
| ------------- |:-------------:| -------------:| 
| blockchain    | BTC  |  https://blockchain.info |
| bitcoinaverage    | BTC  |  https://api.bitcoinaverage.com |
| coinbase    | BTC  |  https://coinbase.com |
| bitstamp    | BTC  |  https://bitstamp.com |
| bitfinex    | BTC,LTC  |  https://api.bitfinex.com |
| btce  | BTC, PPC, LTC  |  https://btc-e.com |
| bter    | BTC  |  http://data.bter.com |
| ccedk    | BTC |  https://www.ccedk.com |
| coinmarketcap_no    | PPC,ETH,XRP,LTC |  http://coinmarketcap.northpole.ro |
| coinmarketcap_ne    | PPC,ETH,XRP,LTC |  http://coinmarketcap-nexuist.rhcloud.com |
| ripplecharts    | XRP |  https://www.ripplecharts.com |
| kraken    | BTC,ETH |  http://kraken.com |
| poloniex    | ETH,XRP,LTC |  http://poloniex.com |
| gatecoin    | ETH |  http://gatecoin.com |
| BitstampEUR    | EUR  |  https://bitstamp.com |
| yahoo    | EUR,CNY,PHP,HKD ...  |  https://yahooapis.com |
| Openexchangerates    | EUR,CNY, PHP,HKD...  |  https://openexchangerates.org |
| Exchangeratelab    | EUR,CNY, PHP,HKD..  |  https://exchangeratelab.com
| GoogleOfficial    | EUR,CNY, PHP,HKD..  |  http://www.google.com/finance/

#Old feeds now unsupported
| GoogleUnofficialPrice    | EUR,CNY,PHP,HKD ...  |  http://rate-exchange.appspot.com |

