# NuBot contributions

**This file is a draft**


## Development process

The git repository for NuBot is located at https://bitbucket.org/JordanLeePeershares/nubottrading

Pull requests are welcome. To be able to submit pull requests an account with Bitbucket is needed.

NuBot development process follows the git-flow model. For more information see http://nvie.com/posts/a-successful-git-branching-model/

Features are developed in new branches assigned to a Bitbucket issue, i.e. branch feature/123 is development with regards to issue 123.

Critical changes shall be submitted via Online Pull Requests. This allows for code comments and discussion. The merge is done by person other than the person who submitted the PR, usually the last to approve.

## Dependencies

Make sure Java 8 is installed (java -version). See: http://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html

Dependencies are located in the lib folder (gradle in the future).

Grip is used to compile markdown into HTML, see https://github.com/joeyespo/grip

Gradle is needed to build.

## Language and IDE

NuBot is developed in Java8. The recommendation for an IDE is IntelliJ 14.



## Test Exchange

A test exchange is located at http://178.62.186.229. Accounts for testing can be created on request.

## Release process 
Refer to RELEASE-PROCESS.md
## Nofications

HipChat or Gitter is used for notifications, along with emails.

## Jenkins

Jenkins is used. http://build.mj2p.co.uk/

TODO: more info here.

## Trello

currently not in use. Coordination takes place over the forum or Hipchat or Bitbucket.

