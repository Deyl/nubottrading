##Available exchanges

| Exchange name        | status   |  URL |
| ------------- |:-------------:| -------------:| 
| [AllCoin](https://www.allcoin.com)    | live  |  https://www.allcoin.com |
| [BitcoinCoID](https://vip.bitcoin.co.id)    | live  |  https://vip.bitcoin.co.id |
| [Poloniex](https://poloniex.com)    | live  |  https://poloniex.com |
| [Bittrex](https://bittrex.com)    | live  |  https://bittrex.com |
| [CCEDK](https://ccedk.com)    | live  |  https://ccedk.com |
| [Bter](https://bter.com)    | live  |  https://bter.com |
| [AltsTrade](https://alts.trade)    | live  |  https://alts.trade |
| [Hitbtc](https://hitbtc.com)    | live  |  https://hitbtc.com |
| [Cryptsy](https://cryptsy.com)    | live  |  https://cryptsy.com |
| [SouthXChange](https://southxchange.com)    | live  |  https://southxchange.com |
| [Peatio]    | test exchange  |  live | https://178.62.140.24/ |

not operational for liquidity operations

| Exchange name        | status   |  URL |
| ------------- |:-------------:| -------------:| 
| Btce    | datafeed only  |   |
| [ShapeShift](https://shapeshift.io)    | no nubot operations  | https://shapeshift.io  |
| CCEX    |  delisted nubits |   |
| Excoin    | defunct  |   |
| [Comkort](https://comkort.com)    | delisted nubits  |  https://comkort.com |
| [Bitspark](https://www.bitspark.io)    | shifted business |  https://www.bitspark.io |



